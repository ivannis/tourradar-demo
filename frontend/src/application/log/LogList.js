import createRemoteObject from "../core/RemoteObject";
import { query } from '../core/effects';
import gql from './gql';

const LogList = createRemoteObject({
    namespace: 'log', store: 'list'
}).extend({
    selectors: (module) => ({
        all: (state) => module.selectors.data(state),
        getById: (state, id) => module.select(state, (localState) => localState.data.find(log => log.id === id))
    }),
    sagas: () => ({
        doFetch: function *doFetch() {
            const data = yield query(gql.queries.logs)

            return data.logs;
        }
    })
})

export default LogList;
