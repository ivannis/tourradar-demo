import gql from 'graphql-tag';

export default gql`
    query logs {
        logs {
            id
            dataset {
              name
            }
            level
            message
            createdAt
        }
    }
`