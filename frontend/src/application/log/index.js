import Module from "../core/Module";
import LogList from './LogList'

const Log = new Module({
    namespace: 'app', store: 'log',
    modules: [
        LogList
    ],
    initialState: {},
})

export default Log;