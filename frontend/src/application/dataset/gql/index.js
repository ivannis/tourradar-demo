import datasets from './datasets';
import stats from './stats';

export default {
    queries: {
        datasets,
        stats,
    },
};
