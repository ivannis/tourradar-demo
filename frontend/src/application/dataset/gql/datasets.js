import gql from 'graphql-tag';

export default gql`
    query datasets {
        datasets {
            id
            name
            urls {
              format
              url
            }
            downloads {
              downloadedAt
              format
              state
            }
            downloadsStats {
              count
              successful
              failed
            }
            createdAt
            updatedAt
        }
    }
`