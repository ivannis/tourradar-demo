import gql from 'graphql-tag';

export default gql`
    query stats {
        stats {
            count
            failed
            successful
        }
          
        logs {    
            level
            createdAt
        }
    }
`