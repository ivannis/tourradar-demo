import createRemoteObject from "../core/RemoteObject";
import { query } from '../core/effects';
import gql from './gql';

const DatasetDownloadStats = createRemoteObject({
    namespace: 'dataset', store: 'stats'
}).extend({
    selectors: (module) => ({
        all: (state) => module.selectors.data(state),
        stats: (state) => module.selectors.data(state).stats,
        logs: (state) => module.selectors.data(state).logs
    }),
    sagas: () => ({
        doFetch: function *doFetch() {
            const data = yield query(gql.queries.stats)

            return data;
        }
    })
})

export default DatasetDownloadStats;
