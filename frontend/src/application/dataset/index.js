import Module from "../core/Module";
import DatasetList from './DatasetList'
import DatasetDownloadStats from './DatasetDownloadStats'

const Dataset = new Module({
    namespace: 'app', store: 'dataset',
    modules: [
        DatasetList,
        DatasetDownloadStats,
    ],
    initialState: {},
})

export default Dataset;