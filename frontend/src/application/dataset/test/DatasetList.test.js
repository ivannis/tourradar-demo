import remoteObjectTest from '../../core/test/RemoteObject.test';
import DatasetList from '../DatasetList';

describe('Dataset list', () => {
    remoteObjectTest(DatasetList, [{ id: 1, name: 'DDD conference', availableTickets: 32 }, { id: 2, name: 'ReactJS conference', availableTickets: 250 }]);
});
