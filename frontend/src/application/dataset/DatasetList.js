import createRemoteObject from "../core/RemoteObject";
import { query } from '../core/effects';
import gql from './gql';

const DatasetList = createRemoteObject({
    namespace: 'dataset', store: 'list'
}).extend({
    selectors: (module) => ({
        all: (state) => module.selectors.data(state),
        getById: (state, id) => module.select(state, (localState) => localState.data.find(dataset => dataset.id === id))
    }),
    sagas: () => ({
        doFetch: function *doFetch() {
            const data = yield query(gql.queries.datasets)

            return data.datasets;
        }
    })
})

export default DatasetList;
