import React from "react";
import { connect } from "react-redux";
import { FormattedMessage } from 'react-intl'
import { Table, Alert } from 'antd';
import moment from "moment";
import App from "../../../application";

const mapping = {
    'emergency': 'error',
    'alert': 'error',
    'critical': 'error',
    'error': 'error',
    'warning': 'warning',
    'notice': 'info',
}

const columns = [{
    title: 'Level',
    dataIndex: 'level',
    render: level => <Alert message={level} type={mapping[level]} />
}, {
    title: 'Dataset',
    dataIndex: 'dataset',
    render: dataset => dataset.name
}, {
    title: 'Message',
    dataIndex: 'message'
}, {
    title: 'Ocurred On',
    dataIndex: 'createdAt',
    render: timestamp => moment.unix(timestamp).format('MMMM Do YYYY, h:mm:ss a')
}];

class LogList extends React.Component {
    constructor(props) {
        super(props)

        this.props.fetch();
    }

    render() {
        const { logs, isLoading } = this.props;

        return (
            <div>
                <style global="true" jsx="true">{`
                    .ant-alert {
                        text-align: center;
                    }
                `}
                </style>
                <h1><FormattedMessage id='title.log.list' defaultMessage='Log list' /></h1>
                <Table rowKey="id" columns={columns} dataSource={logs} loading={isLoading} />
            </div>
        );
    }
}

const actions = App.actions.log.list;
const selectors = App.selectors.log.list;

const mapStateToProps = (state) => ({
    logs: selectors.all(state),
    isLoading: selectors.isLoading(state),
});

const mapDispatchToProps = (dispatch) => ({
    fetch: () => dispatch(actions.fetch()),
});

export default connect(mapStateToProps, mapDispatchToProps)(LogList)