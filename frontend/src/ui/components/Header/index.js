import LightHeader from './Light'
import DarkHeader from './Dark'

export { DarkHeader }
export default LightHeader