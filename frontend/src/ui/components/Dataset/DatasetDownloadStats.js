import React from "react";
import { FormattedMessage } from 'react-intl'
import { Card, Col, Row } from 'antd';
import PieChart from "./PieChart";
import BarChart from "./BarChart";

export default class DatasetDownloadStats extends React.Component {
    render() {
        return (
            <div>
                <h1><FormattedMessage id='title.dataset.stats' defaultMessage='Dataset stats' /></h1>
                <Row gutter={16}>
                    <Col span={12}>
                        <Card bordered={false}>
                            <PieChart />
                        </Card>
                    </Col>
                    <Col span={12}>
                        <Card bordered={false}>
                            <BarChart />
                        </Card>
                    </Col>
                </Row>
            </div>
        );
    }
}