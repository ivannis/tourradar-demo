import React from "react";
import { connect } from "react-redux";
import { FormattedMessage } from 'react-intl'
import { Table } from 'antd';
import moment from "moment";
import App from "../../../application";

const columns = [{
    title: 'Dataset',
    dataIndex: 'name',
    render: text => <a href="javascript:;">{text}</a>,
}, {
    title: 'Formats',
    dataIndex: 'urls',
    render: urls => urls.map(item => item.format).join(', '),
}, {
    title: 'Createt At',
    dataIndex: 'createdAt',
    render: timestamp => moment.unix(timestamp).format('MMMM Do YYYY, h:mm:ss a')
}, {
    title: 'Last update',
    dataIndex: 'updatedAt',
    render: timestamp => moment.unix(timestamp).format('MMMM Do YYYY, h:mm:ss a')
}, {
    title: 'Downloads',
    dataIndex: 'downloadsStats',
    render: stats => stats.count,
}, {
    title: 'Failed Downloads',
    dataIndex: 'downloadsStats',
    render: stats => stats.failed,
}, {
    title: 'Successful Downloads',
    dataIndex: 'downloadsStats',
    render: stats => stats.successful,
}];

class DatasetList extends React.Component {
    constructor(props) {
        super(props)

        this.props.fetch();
    }

    render() {
        const { datasets, isLoading } = this.props;

        return (
            <div>
                <h1><FormattedMessage id='title.dataset.list' defaultMessage='Dataset list' /></h1>
                <Table rowKey="id" columns={columns} dataSource={datasets} loading={isLoading} />
            </div>
        );
    }
}

const actions = App.actions.dataset.list;
const selectors = App.selectors.dataset.list;

const mapStateToProps = (state) => ({
    datasets: selectors.all(state),
    isLoading: selectors.isLoading(state),
});

const mapDispatchToProps = (dispatch) => ({
    fetch: () => dispatch(actions.fetch()),
});

export default connect(mapStateToProps, mapDispatchToProps)(DatasetList)