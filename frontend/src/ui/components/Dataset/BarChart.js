import React from "react";
import { connect } from "react-redux";
import {Bar} from 'react-chartjs-2';
import App from "../../../application";

class BarChart extends React.Component {
    constructor(props) {
        super(props)

        this.props.fetch();
    }

    render() {
        const { stats, isLoading } = this.props;

        if (isLoading || stats === undefined) {
            return <div>
                Loading
            </div>
        }

        const data = {
            labels: [
                'Count',
                'Failed',
                'Successful'
            ],
            datasets: [{
                label: 'Dataset download stats',
                data: [stats.count, stats.failed, stats.successful],
                backgroundColor: [
                    '#36A2EB',
                    '#FF6384',
                    '#4BC0C0',
                ],
                hoverBackgroundColor: [
                    '#36A2EB',
                    '#FF6384',
                    '#4BC0C0',
                ]
            }]
        };

        const options = {
            scales: {
                yAxes: [{
                    ticks: {
                        min: 0
                    }
                }]
            }
        }

        return (
            <Bar data={data} options={options} />
        );
    }
}


const actions = App.actions.dataset.stats;
const selectors = App.selectors.dataset.stats;

const mapStateToProps = (state) => ({
    stats: selectors.stats(state),
    isLoading: selectors.isLoading(state),
});

const mapDispatchToProps = (dispatch) => ({
    fetch: () => dispatch(actions.fetch()),
});

export default connect(mapStateToProps, mapDispatchToProps)(BarChart)