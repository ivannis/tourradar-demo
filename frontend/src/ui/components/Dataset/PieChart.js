import React from "react";
import { connect } from "react-redux";
import { FormattedMessage } from 'react-intl'
import {Doughnut} from 'react-chartjs-2';
import { Card } from 'antd';
import moment from "moment";
import App from "../../../application";

class PieChart extends React.Component {
    constructor(props) {
        super(props)

        this.props.fetch();
    }

    render() {
        const { stats, isLoading } = this.props;

        if (isLoading || stats === undefined) {
            return <div>
                Loading
            </div>
        }

        const data = {
            labels: [
                'Downloads count',
                'Downloads failed',
                'Downloads successful'
            ],
            datasets: [{
                data: [stats.count, stats.failed, stats.successful],
                backgroundColor: [
                    '#36A2EB',
                    '#FF6384',
                    '#4BC0C0',
                ],
                hoverBackgroundColor: [
                    '#36A2EB',
                    '#FF6384',
                    '#4BC0C0',
                ]
            }]
        };

        return (
            <Doughnut data={data} />
        );
    }
}


const actions = App.actions.dataset.stats;
const selectors = App.selectors.dataset.stats;

const mapStateToProps = (state) => ({
    stats: selectors.stats(state),
    isLoading: selectors.isLoading(state),
});

const mapDispatchToProps = (dispatch) => ({
    fetch: () => dispatch(actions.fetch()),
});

export default connect(mapStateToProps, mapDispatchToProps)(PieChart)