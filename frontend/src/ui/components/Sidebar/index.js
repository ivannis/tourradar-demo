import React from "react";
import { FormattedMessage } from 'react-intl'
import { Layout, Menu, Icon } from 'antd';
import Link from '../Link'
import {connect} from "react-redux";
import App from "../../../application";

const { Sider } = Layout;
const styles = `       
    .logo-container {
        position: relative;
        height: 64px;
        background: #002140;
    }
    
    .logo {
        cursor: pointer;
        left: 24px;
        top: 10px;
        background: #e40079;
        width: 36px;
        height: 36px;
        display: block;
        position: absolute;
        border-top-left-radius: 20px;
        border-top-right-radius: 20px;
        border-bottom-left-radius: 20px;
        -webkit-transform: rotate(45deg);
        -moz-transform: rotate(45deg);
        transform: rotate(45deg);
    }
    
    .brand-name {
        cursor: pointer;
        font-weight: bold;
        color: #fff;
        font-size: 20px;
        position: absolute;
        left: 20px;
        top: 18px;
        width: 170px;
        display: block;
    }
`

function isObject(val) {
    if (val === null || Array.isArray(val)) { return false;}

    return ( (typeof val === 'function') || (typeof val === 'object') );
}

class Sidebar extends React.Component {
    state = {
        collapsed: false,
    };

    toggle = () => {
        this.setState({
            collapsed: !this.state.collapsed,
        });
    }

    onCollapse = (collapsed) => {
        this.setState({ collapsed });
    }

    isAdmin = (user) => {
        if (isObject(user)) {
            return user.permissions.some(permission => permission == 'app');
        }

        return false;
    }

    constructor(props) {
        super(props)

        this.props.fetch();
    }

    render() {
        const { currentUser } = this.props;

        return (
            <Sider
                collapsible
                collapsed={this.state.collapsed}
                onCollapse={this.onCollapse}
                className="sidebar"
            >
                <style jsx="true">{styles}</style>
                <style jsx="true">
                    {`
                        .logo::before{
                            width: 20px;
                            height: 20px;
                            display:block;
                            border:5px solid #F7F5F2;
                            content:"";
                            position:absolute;
                            border-radius:14px;
                            top:8px;
                            left:8px;
                        }
                    `}
                </style>
                <style global="true" jsx="true">
                    {`
                        .ant-layout-sider-trigger {
                          transition: color .3s;
                        }

                        .ant-layout-sider-trigger:hover {
                            color: #1890ff;
                        }

                        .ant-dropdown-menu-item .anticon {
                            margin-right: 5px;
                        }
                    `}
                </style>
                <Link route='home'>
                    <div className="logo-container">
                        <div className="brand-name">Tourradar demo</div>
                    </div>
                </Link>

                { this.isAdmin(currentUser) ? (
                    <Menu defaultSelectedKeys={['1']} mode="inline" theme="dark">
                        <Menu.Item key="1">
                            <Icon type="dashboard" />
                            <Link route='home'><span><FormattedMessage id='nav.dashboard' defaultMessage='Dashboard' /></span></Link>
                        </Menu.Item>
                        <Menu.Item key="2">
                            <Icon type="link" />
                            <Link route='dataset_list'><span><FormattedMessage id='nav.datasets' defaultMessage='Datasets' /></span></Link>
                        </Menu.Item>
                        <Menu.Item key="3">
                            <Icon type="exception" />
                            <Link route='log_list'><span><FormattedMessage id='nav.logs' defaultMessage='Logs' /></span></Link>
                        </Menu.Item>
                    </Menu>
                ) : (
                    <Menu defaultSelectedKeys={['1']} mode="inline" theme="dark">
                        <Menu.Item key="1">
                            <Icon type="dashboard" />
                            <Link route='home'><span><FormattedMessage id='nav.dashboard' defaultMessage='Dashboard' /></span></Link>
                        </Menu.Item>
                        <Menu.Item key="2">
                            <Icon type="exception" />
                            <Link route='log_list'><span><FormattedMessage id='nav.logs' defaultMessage='Logs' /></span></Link>
                        </Menu.Item>
                    </Menu>
                )}
            </Sider>
        );
    }
}

const actions = App.actions.security.user.authenticated;
const selectors = App.selectors.security.user.authenticated;

const mapStateToProps = (state) => ({
    currentUser: selectors.me(state),
});

const mapDispatchToProps = (dispatch) => ({
    fetch: () => dispatch(actions.fetch()),
});

export default connect(mapStateToProps, mapDispatchToProps)(Sidebar)