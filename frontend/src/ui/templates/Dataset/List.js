import React from 'react';
import Page from '../../components/Page'
import Layout from '../../components/Layout'
import DatasetList from '../../components/Dataset/DatasetList'

export default class DatasetListPage extends React.Component {
    render() {
        return (
            <Page title="Datasets" description="System dataset list">
                <Layout>
                    <DatasetList></DatasetList>
                </Layout>
            </Page>
        );
    }
}

