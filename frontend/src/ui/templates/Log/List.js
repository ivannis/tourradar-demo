import React from 'react';
import Page from '../../components/Page'
import Layout from '../../components/Layout'
import LogList from '../../components/Log/LogList'

export default class LogListPage extends React.Component {
    render() {
        return (
            <Page title="Logs" description="System dataset list">
                <Layout>
                    <LogList></LogList>
                </Layout>
            </Page>
        );
    }
}

