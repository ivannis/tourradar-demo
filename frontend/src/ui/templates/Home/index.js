import React from 'react';
import Page from '../../components/Page'
import Layout from '../../components/Layout'
import DatasetDownloadStats from '../../components/Dataset/DatasetDownloadStats'

export default class HomePage extends React.Component {
    render() {
        return (
            <Page title="Dashboard" description="Dashboard description">
                <Layout>
                    <DatasetDownloadStats />
                </Layout>
            </Page>
        );
    }
}

