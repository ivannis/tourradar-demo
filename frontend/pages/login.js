import { withReduxSaga } from "../src/infrastructure/with";
import LoginPage from '../src/ui/templates/Security/Login'

export default withReduxSaga(LoginPage)