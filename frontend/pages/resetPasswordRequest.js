import { withReduxSaga } from "../src/infrastructure/with";
import ResetPasswordRequestPage from '../src/ui/templates/Security/ResetPasswordRequest'

export default withReduxSaga(ResetPasswordRequestPage)