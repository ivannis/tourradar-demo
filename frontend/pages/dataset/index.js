import { withReduxSagaAndSecurity } from "../../src/infrastructure/with";
import DatasetListPage from '../../src/ui/templates/Dataset/List'

export default withReduxSagaAndSecurity(DatasetListPage)