import { withReduxSaga } from "../src/infrastructure/with";
import RegisterPage from '../src/ui/templates/Security/Register'

export default withReduxSaga(RegisterPage)