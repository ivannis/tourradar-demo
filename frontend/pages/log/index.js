import { withReduxSagaAndSecurity } from "../../src/infrastructure/with";
import LogListPage from '../../src/ui/templates/Log/List'

export default withReduxSagaAndSecurity(LogListPage)