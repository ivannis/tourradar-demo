import { withReduxSagaAndSecurity } from "../src/infrastructure/with";
import HomePage from '../src/ui/templates/Home'

export default withReduxSagaAndSecurity(HomePage)