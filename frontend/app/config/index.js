module.exports = {
    'process.env.api': {
        url: process.env.API_URL || 'http://api.tourradar.local/graphql'
    },
    'process.env.defaultLanguage': 'en_US'
};