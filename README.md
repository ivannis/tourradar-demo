# Tourradar Demo

The tourradar demo project it was divided into 3 applications:

**Console**: A console application that contains the resolution of the problem-1, problem-3 and problem-4.

**Backend**: A backend-api application that contains the resolution of the problem-0.

**Frontend**: A small frontend application with React + Redux + Redux Saga to show the download statistics and the logs.


## Getting Started

These instructions will get you a copy of the project up and running on your local machine for testing purposes.

### Prerequisites

Clone the repository

```bash
git clone https://gitlab.com/ivannis/tourradar-demo.git
```

### Installing

- Install the [console application](https://gitlab.com/ivannis/tourradar-demo/blob/master/console/README.md)
- Install the [backend application](https://gitlab.com/ivannis/tourradar-demo/blob/master/backend/README.md)
- Install the [frontend application](https://gitlab.com/ivannis/tourradar-demo/blob/master/frontend/README.md)

## Authors

* [Ivan Suarez Jerez](https://github.com/ivannis)