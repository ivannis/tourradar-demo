# Console

The console project contains the resolution of the problem-1, problem-3 and problem-4.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for testing purposes.

### Installing

```bash
cd tourradar-demo
docker-compose up -d
docker exec -it tourradar-demo /www-data-shell
# now you are inside the tourradar-demo container
composer install --prefer-dist
```

## What can we do?

You have a sets of command to test the different algorithms.

### Problem 1

> *Given an array of integers sorted in ascending order, find the starting and ending position of a given target value.*

Use the following commands to test it:

```bash
- bin/console app:search 5, 7, 7, 8, 8, 10 --target=8
- bin/console app:search-random
```

### Problem 3

> *Given stock on day X, design an algorithm to find the maximum profit.*

Use the following commands to test it:

```bash
- bin/console app:maximum-profit 7, 1, 5, 3, 6, 4
- bin/console app:maximum-profit-random
```

### Problem 4

> *Find all valid IP address combinations within a given string containing only digits.*

Use the following commands to test it:

```bash
- bin/console app:ip-address-combinations "25525511135"
- bin/console app:ip-address-combinations-random --length=LENGTH --max=MAXIMUN
```

## Tests

### Running all the unit tests suite

```bash
bin/atoum 
```

### Running all the tests coverage

```bash
# Run the tests coverage
# Make sure the /var/www/coverage directory is world writable
bin/atoum -c .atoum.html.php
# Check it at coverage.tourradar.local
```

## Built With

* [Symfony console](https://symfony.com)

## Authors

* [Ivan Suarez Jerez](https://github.com/ivannis)