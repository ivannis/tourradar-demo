<?php

namespace Problem;

/**
 * Finish implementation of Class Problem4 by having the method it must implement return the
 * solution to the following problem:
 *
 * Find all valid IP address combinations within a given string containing only digits.
 *
 * For example:
 *
 * $params[0] = '25525511135'
 * return ['255.255.11.135', '255.255.111.35']
 *
 */
class Problem4 implements Problem
{
    /**
     * {@inheritdoc}
     */
	public function run(...$params)
	{
        return $this->addresses($params[0]);
	}

    /**
     * @param string $string
     * @param int    $level
     *
     * @return array
     */
    private function addresses(string $string, int $level=3): array
    {
        $ips = [];
        for($i = 1; $i <= 3; $i++) {
            $start = $this->slot($string, 0, $i);

            if ($this->validSlot($start)) {
                $end = $this->slot($string, $i);

                if (!$this->emptySlot($end)) {
                    if ($level == 1) {
                        if ($this->validSlot($end)) {
                            $ips[] = intval($start) . "." . intval($end);
                        }
                    } else {
                        foreach ($this->addresses($end, $level - 1) as $remaining) {
                            $ips[] = intval($start) . "." . $remaining;
                        }
                    }
                }
            }
        }

        return $ips;
    }

    /**
     * @param string   $string
     * @param int      $start
     * @param int|null $length
     *
     * @return string
     */
    private function slot(string $string, int $start, int $length=null): string
    {
        $slot = mb_substr($string, $start, $length);

        return $slot === false ? "" : $slot;
    }

    /**
     * @param string $slot
     *
     * @return bool
     */
    private function emptySlot(string $slot): bool
    {
        return empty($slot) && strlen($slot) === 0;
    }

    /**
     * @param string $slot
     *
     * @return bool
     */
    private function validSlot(string $slot): bool
    {
        if (intval($slot) < 256) {
            if ($slot[0] == 0 && strlen($slot) > 1) {
                return false;
            }

            return true;
        }

        return false;
    }
}
