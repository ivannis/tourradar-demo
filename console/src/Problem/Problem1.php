<?php

namespace Problem;

/**
 * Finish implementation of Class Problem1 by having the method it must implement return the
 * solution to the following problem:
 *
 * Given an array of integers sorted in ascending order, find the starting and ending position of a given target value.
 *
 * Your algorithm's runtime complexity must be in the order of O(log n).
 *
 * If the target is not found in the array, return [-1, -1].
 *
 * For example, given
 * $params[0] = [5, 7, 7, 8, 8, 10]
 * and target value
 * $params[1] = 8
 *
 * return [3, 4]
 *
 */
class Problem1 implements Problem
{
    /**
     * {@inheritdoc}
     */
    public function run(...$params)
    {
        $dataset = $params[0];
        $target = $params[1];

        return $this->bsearch($dataset, $target, 0, count($dataset) - 1, true, true);
    }

    /**
     * @param array $dataset
     * @param int   $target
     * @param int   $from
     * @param int   $to
     * @param bool  $findFirst
     * @param bool  $findLast
     *
     * @return array
     */
    private function bsearch(array $dataset, int $target, int $from, int $to, bool $findFirst, bool $findLast): array
    {
        if ($from > $to) { // not found
            return [-1, -1];
        }

        if ($from === $to) {
            return $dataset[$from] === $target ? [$from, $to] : [-1, -1];
        }

        $middle = (int) (($from + $to)/2);
        if ($dataset[$middle] > $target) {
            return $this->bsearch($dataset, $target, $from, $middle - 1, $findFirst, $findLast);
        }

        if ($dataset[$middle] < $target) {
            return $this->bsearch($dataset, $target, $middle + 1, $to, $findFirst, $findLast);
        }

        // $dataset[$middle] === $target
        $first = -1;
        $last = -1;
        if ($findFirst) { // binary search to find the first one to the left
            $first = $this->bsearch($dataset, $target, $from, $middle - 1, true, false)[0];
        }

        if ($findLast) { // binary search to find the last one to the right
            $last = $this->bsearch($dataset, $target, $middle + 1, $to, false, true)[1];
        }

        return [$first === -1 ? $middle : $first, $last === -1 ? $middle : $last];
    }
}