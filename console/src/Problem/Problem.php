<?php
namespace Problem;

interface Problem
{
    public function run(...$params);
}
