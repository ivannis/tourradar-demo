<?php

namespace Problem;

/**
 * Finish implementation of Class Problem3 by having the method it must implement return the
 * solution to the following problem:
 *
 * You have an array for which the ith element is the price of a given stock on day i.
 *
 * If you were only permitted to complete at most one transaction (ie, buy one and sell one share of the stock),
 * design an algorithm to find the maximum profit.
 *
 * For example:
 *
 * $params[0] = [7, 1, 5, 3, 6, 4]
 * return 5
 *
 * $params[0] = [7, 6, 4, 3, 1]
 * return 0
 *
 */
class Problem3 implements Problem
{
    /**
     * {@inheritdoc}
     */
    public function run(...$params)
    {
        return $this->maxProfit($params[0]);
    }

    /**
     * @param array $prices
     *
     * @return int
     */
    private function maxProfit(array $prices): int
    {
        $profit = 0;
        $min = PHP_INT_MAX;

        for ($i = 0; $i < count($prices); ++$i) {
            $profit = max($profit, $prices[$i] - $min);
            $min = min($min, $prices[$i]);
        }

        return $profit;
    }
}