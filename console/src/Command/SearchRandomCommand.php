<?php
namespace Command;

use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * SearchRandomCommand class.
 *
 * @author Ivan Suárez Jerez <ivannis.suarez@gmail.com>
 */
class SearchRandomCommand extends Command
{
    /**
     * {@inheritdoc}
     */
    public function configure()
    {
        $this->setName('app:search-random')
            ->setDescription('Generate random data to test the app:search command.')
            ->addOption('length', '', InputOption::VALUE_REQUIRED, 'The collection length', 100)
            ->addOption('max', '', InputOption::VALUE_REQUIRED, 'The maximun number', 20)
        ;
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return int|null|void
     */
    public function execute(InputInterface $input, OutputInterface $output)
    {
        $length = $input->getOption('length');
        $max = $input->getOption('max');

        $collection = [];
        $target = rand(0, $max);

        for($i = 0; $i < $length; $i++) {
            $collection[] = rand(0, $max);
        }
        sort($collection);

        $commandInput = new ArrayInput(array(
            'command'    => 'app:search',
            'collection' => $collection,
            '--target'   => $target
        ));

        $this
            ->runCommand('app:search', $commandInput, $output)
        ;
    }
}