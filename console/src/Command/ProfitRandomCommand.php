<?php
namespace Command;

use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * ProfitRandomCommand class.
 *
 * @author Ivan Suárez Jerez <ivannis.suarez@gmail.com>
 */
class ProfitRandomCommand extends Command
{
    /**
     * {@inheritdoc}
     */
    public function configure()
    {
        $this->setName('app:maximum-profit-random')
            ->setDescription('Generate random data to test the app:maximum-profit command.')
            ->addOption('length', '', InputOption::VALUE_REQUIRED, 'The collection length', 15)
            ->addOption('max', '', InputOption::VALUE_REQUIRED, 'The maximun number line', 10)
        ;
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return int|null|void
     */
    public function execute(InputInterface $input, OutputInterface $output)
    {
        $length = $input->getOption('length');
        $max = $input->getOption('max');

        $collection = [];
        for($i = 0; $i < $length; $i++) {
            $collection[] = rand(1, $max);
        }

        $commandInput = new ArrayInput(array(
            'command' => 'app:maximum-profit',
            'prices'  => $collection
        ));

        $this
            ->runCommand('app:maximum-profit', $commandInput, $output)
        ;
    }
}