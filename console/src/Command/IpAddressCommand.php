<?php
namespace Command;

use Problem\Problem4;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * IpAddressCommand class.
 *
 * @author Ivan Suárez Jerez <ivannis.suarez@gmail.com>
 */
class IpAddressCommand extends Command
{
    /**
     * {@inheritdoc}
     */
    public function configure()
    {
        $this->setName('app:ip-address-combinations')
            ->setDescription('Find all valid IP address combinations.')
            ->addArgument('string', InputArgument::IS_ARRAY | InputArgument::REQUIRED, 'The string of digits')
        ;
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return int|null|void
     */
    public function execute(InputInterface $input, OutputInterface $output)
    {
        $problem = new Problem4();
        foreach ($input->getArgument('string') as $string) {
            $addresses = $problem->run($string);

            if (count($addresses) === 0) {
                $output->writeln(sprintf(
                    '<error>%s </error> "%s"',
                    'There is not any valid ip address for the string',
                    $string
                ));
            } else {
                $output->writeln(sprintf(
                    '<comment>%d</comment> <info>%s</info> "%s" <info>%s</info>',
                    count($addresses),
                    'valid ip address for the string',
                    $string,
                    'was found.'
                ));

                foreach ($addresses as $ipAddress) {
                    $output->writeln(sprintf(
                        '<comment>%s</comment>',
                        $ipAddress
                    ));
                }
            }
        }
    }
}