<?php
namespace Command;

use Problem\Problem1;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * SearchCommand class.
 *
 * @author Ivan Suárez Jerez <ivannis.suarez@gmail.com>
 */
class SearchCommand extends Command
{
    /**
     * {@inheritdoc}
     */
    public function configure()
    {
        $this->setName('app:search')
            ->setDescription('Find the starting and ending position of a given target value.')
            ->addArgument('collection', InputArgument::IS_ARRAY | InputArgument::REQUIRED, 'The collection of digits')
            ->addOption('target', '', InputOption::VALUE_REQUIRED, 'The target value', 0)
        ;
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return int|null|void
     */
    public function execute(InputInterface $input, OutputInterface $output)
    {
        $collection = $input->getArgument('collection');
        $target = $input->getOption('target');

        $output->writeln(
            sprintf(
                '<info>%s</info> <comment>%d</comment> <info>%s</info> [%s]',
                'Finding',
                $target,
                'in the array collection',
                implode(",", $collection)
            )
        );

        $problem = new Problem1();
        $range = $problem->run($collection, $target);

        $output->writeln(
            sprintf(
                '<info>%s</info> <comment>[%d, %d]</comment> <info>%s</info>',
                'The range',
                $range[0],
                $range[1],
                'was found.'
            )
        );
    }
}