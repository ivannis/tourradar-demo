<?php
namespace Command;

use Problem\Problem3;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * ProfitCommand class.
 *
 * @author Ivan Suárez Jerez <ivannis.suarez@gmail.com>
 */
class ProfitCommand extends Command
{
    /**
     * {@inheritdoc}
     */
    public function configure()
    {
        $this->setName('app:maximum-profit')
            ->setDescription('Find the maximum profit of a given array with the stock of the day.')
            ->addArgument('prices', InputArgument::IS_ARRAY | InputArgument::REQUIRED, 'The list of prices per day')
        ;
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return int|null|void
     */
    public function execute(InputInterface $input, OutputInterface $output)
    {
        $prices = $input->getArgument('prices');

        $problem = new Problem3();
        $profit = $problem->run($prices);

        $output->writeln(
            sprintf(
                '<info>%s </info>[%s]<info> is:</info> <comment>%d €</comment> ',
                'The maximun profit for the given stock',
                implode(",", $prices),
                $profit
            )
        );
    }
}