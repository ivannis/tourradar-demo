<?php
namespace Command;

use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * IpAddressRandomCommand class.
 *
 * @author Ivan Suárez Jerez <ivannis.suarez@gmail.com>
 */
class IpAddressRandomCommand extends Command
{
    /**
     * {@inheritdoc}
     */
    public function configure()
    {
        $this->setName('app:ip-address-combinations-random')
            ->setDescription('Generate random data to test the app:ip-address-combinations command.')
            ->addOption('length', '', InputOption::VALUE_REQUIRED, 'The string length', 15)
            ->addOption('max', '', InputOption::VALUE_REQUIRED, 'The maximun number', 9)
        ;
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return int|null|void
     */
    public function execute(InputInterface $input, OutputInterface $output)
    {
        $length = $input->getOption('length');
        $max = $input->getOption('max');

        $string = [];
        for($i = 0; $i < rand(3, $length); $i++) {
            $string[] = rand(0, $max);
        }

        $commandInput = new ArrayInput(array(
            'command' => 'app:ip-address-combinations',
            'string'  => array(implode("", $string))
        ));

        $this
            ->runCommand('app:ip-address-combinations', $commandInput, $output)
        ;
    }
}