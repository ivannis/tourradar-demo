<?php

/**
 * This file is part of the Tourradar application.
 *
 * Copyright (c) Tourradar
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace AppBundle\Command;

use AppBundle\Command\Core\Command;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * InstallCommand class.
 *
 * @author Ivan Suarez Jerez <ivannis.suarez@gmail.com>
 */
class InstallCommand extends Command
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('app:install')
            ->setDescription('Tourradar install command.')
        ;
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('<info>Installing </info>Tourradar <info>application</info>');

        $this
            ->setupStep($input, $output)
        ;

        $output->writeln('<info>Tourradar application has been successfully installed.</info>');
    }

    /**
     * Setup each step.
     *
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return $this
     */
    protected function setupStep(InputInterface $input, OutputInterface $output)
    {
        return $this
            ->removeDatabases($input, $output)
            ->setupSystem($input, $output)
            ->setupSecurity($input, $output)
        ;
    }

    /**
     * Setup the app system data.
     *
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return $this
     */
    protected function removeDatabases(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('<info>Removing the databases.</info>');

        $commandInput = new ArrayInput(array(
            'command' => 'cubiche:mongodb:schema-drop',
            '--env' => $input->getOption('env'),
        ));

        return $this
            ->runCommand('cubiche:mongodb:schema-drop', $commandInput, $output)
        ;
    }

    /**
     * Setup the app system data.
     *
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return $this
     */
    protected function setupSystem(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('<info>Setting up system.</info>');

        return $this
            ->creatingLanguages($input, $output)
        ;
    }

    /**
     * Setup the app security data.
     *
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return $this
     */
    protected function setupSecurity(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('<info>Setting up security.</info>');

        return $this
            ->creatingRoles($input, $output)
            ->creatingUsers($input, $output)
            ->creatingDatasets($input, $output)
        ;
    }

    /**
     * Creating the language list.
     *
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return $this
     */
    protected function creatingLanguages(InputInterface $input, OutputInterface $output)
    {
        $commandInput = new ArrayInput(array(
            'command' => 'app:language-create-all',
            '--env' => $input->getOption('env'),
        ));

        return $this
            ->runCommand('app:language-create-all', $commandInput, $output)
        ;
    }

    /**
     * Creating the role list.
     *
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return $this
     */
    protected function creatingRoles(InputInterface $input, OutputInterface $output)
    {
        $roles = array('ADMIN' => array('app'), 'MARKETING' => array('app.dataset', 'app.log'));
        foreach ($roles as $roleName => $permissions) {
            $commandInput = new ArrayInput(array(
                'command' => 'app:role-create',
                'name' => $roleName,
                'permissions' => $permissions,
                '--env' => $input->getOption('env'),
            ));

            $this
                ->runCommand('app:role-create', $commandInput, $output)
            ;
        }

        return $this;
    }

    /**
     * Creating the user list.
     *
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return $this
     */
    protected function creatingUsers(InputInterface $input, OutputInterface $output)
    {
        $users = array(
            'ADMIN' => array(
                array('username' => 'admin', 'password' => 'admin', 'email' => 'admin@tourradar.com'),
            ),
            'MARKETING' => array(
                array('username' => 'marketing', 'password' => 'marketing', 'email' => 'marketing@tourradar.com'),
            ),
        );

        foreach ($users as $roleName => $items) {
            foreach ($items as $user) {
                $commandInput = new ArrayInput(array(
                    'command' => 'app:user-create',
                    'username' => $user['username'],
                    'password' => $user['password'],
                    'email' => $user['email'],
                    'roles' => array($roleName),
                    '--env' => $input->getOption('env'),
                ));

                $this
                    ->runCommand('app:user-create', $commandInput, $output)
                ;
            }
        }

        return $this;
    }

    /**
     * Creating the dataset list.
     *
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return $this
     */
    protected function creatingDatasets(InputInterface $input, OutputInterface $output)
    {
        $datasets = array(
            array(
                'name' => 'Demographic Statistics By Zip Code',
                'urls' => array(
                    'json' => 'https://data.cityofnewyork.us/api/views/kku6-nxdu/rows.json?accessType=DOWNLOAD',
                    'xml' => 'https://data.cityofnewyork.us/api/views/kku6-nxdu/rows.xml?accessType=DOWNLOAD',
                    'csv' => 'https://data.cityofnewyork.us/api/views/kku6-nxdu/rows.csv?accessType=DOWNLOAD',
                    'tsv' => 'https://data.cityofnewyork.us/api/views/kku6-nxdu/rows.tsv?accessType=DOWNLOAD'
                )
            ),
            array(
                'name' => 'National Health and Nutrition Examination Survey (NHANES)',
                'urls' => array(
                    'json' => 'https://chronicdata.cdc.gov/api/views/5svk-8bnq/rows.json?accessType=DOWNLOAD',
                    'xml' => 'https://chronicdata.cdc.gov/api/views/5svk-8bnq/rows.xml?accessType=DOWNLOAD',
                    'csv' => 'https://chronicdata.cdc.gov/api/views/5svk-8bnq/rows.csv?accessType=DOWNLOAD',
                    'tsv' => 'https://chronicdata.cdc.gov/api/views/5svk-8bnq/rows.tsv?accessType=DOWNLOAD'
                )
            ),
            array(
                'name' => 'Current Employee Names, Salaries, and Position Titles',
                'urls' => array(
                    'json' => 'https://data.cityofchicago.org/api/views/xzkq-xp2w/rows.json?accessType=DOWNLOAD',
                    'xml' => 'https://data.cityofchicago.org/api/views/xzkq-xp2w/rows.xml?accessType=DOWNLOAD',
                    'csv' => 'https://data.cityofchicago.org/api/views/xzkq-xp2w/rows.csv?accessType=DOWNLOAD',
                    'tsv' => 'https://data.cityofchicago.org/api/views/xzkq-xp2w/rows.tsv?accessType=DOWNLOAD'
                )
            ),
        );

        foreach ($datasets as $dataset) {
            $urls = array_map(function ($key, $value) {
                return $key.','.$value;
            }, array_keys($dataset['urls']), $dataset['urls']);

            $commandInput = new ArrayInput(array(
                'command' => 'app:dataset-create',
                'name'    => $dataset['name'],
                'urls'    => $urls,
                '--env'   => $input->getOption('env'),
            ));

            $this
                ->runCommand('app:dataset-create', $commandInput, $output)
            ;
        }

        return $this;
    }
}
