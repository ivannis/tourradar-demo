<?php

/**
 * This file is part of the Tourradar application.
 *
 * Copyright (c) Tourradar
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace AppBundle\Command\Dataset;

use AppBundle\Command\Core\Command;
use Cubiche\Core\Validator\Exception\ValidationException;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Tourradar\Core\Domain\Exception\NotFoundException;
use Tourradar\Resource\Domain\Command\CreateDatasetCommand;
use Tourradar\Resource\Domain\DatasetId;

/**
 * CreateCommand class.
 *
 * @author Ivan Suarez Jerez <ivannis.suarez@gmail.com>
 */
class CreateCommand extends Command
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('app:dataset-create')
            ->addArgument('name', InputArgument::REQUIRED, 'The dataset name.')
            ->addArgument('urls', InputArgument::IS_ARRAY | InputArgument::REQUIRED, 'The dataset urls.')
            ->setDescription('Creates a new dataset.')
            ->setHelp('This command allows you to create a dataset...')
        ;
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $datasetId = DatasetId::next()->toNative();
        $name = $input->getArgument('name');
        $urls = $this->normalizeUrls($input->getArgument('urls'));

        $output->writeln('<info>Creating a new </info>dataset');
        try {
            $this->commandBus()->dispatch(
                new CreateDatasetCommand(
                    $datasetId,
                    $name,
                    $urls
                )
            );

            $output->writeln(
                '<info>A new dataset with name </info>"'.$name.'"<info> has been successfully created.</info>'
            );
        } catch (NotFoundException $e) {
            $output->writeln('<error>'.$e->getMessage().'</error>');
        } catch (ValidationException $e) {
            $this->printValidationErrors($e, $output);
        }
    }

    /**
     * @param array $values
     *
     * @return array
     */
    private function normalizeUrls($values)
    {
        $name = [];
        foreach ($values as $item) {
            list($format, $url) = explode(',', $item);

            $name[$format] = $url;
        }

        return $name;
    }
}
