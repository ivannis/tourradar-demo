<?php

/**
 * This file is part of the Tourradar application.
 *
 * Copyright (c) Tourradar
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace AppBundle\Command\Dataset;

use AppBundle\Command\Core\Command;
use Cubiche\Core\Bus\Exception\NotFoundException;
use Cubiche\Core\Validator\Exception\ValidationException;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ChoiceQuestion;
use Tourradar\Resource\Domain\Command\AddDatasetUrlCommand;
use Tourradar\Resource\Domain\Command\DownloadDatasetRequestCommand;
use Tourradar\Resource\Domain\ReadModel\Dataset;
use Tourradar\Resource\Domain\ReadModel\Query\FindAllDatasets;

/**
 * SetUrlCommand class.
 *
 * @author Ivan Suarez Jerez <ivannis.suarez@gmail.com>
 */
class SetUrlCommand extends Command
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('app:dataset-url')
            ->addOption('format', '', InputOption::VALUE_REQUIRED, 'The dataset url format')
            ->addOption('url', '', InputOption::VALUE_REQUIRED, 'The dataset url')
            ->setDescription('Set the dataset url.')
        ;
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $helper = $this->getHelper('question');
        $names = [];
        $ids = [];

        $datasets = $this->findAllDatasets();
        foreach ($datasets as $dataset) {
            $names[] = $dataset->name()->toNative();
            $ids[$dataset->name()->toNative()] = $dataset->datasetId()->toNative();
        }

        $question = new ChoiceQuestion('Please select the dataset name', $names, 0);
        $question->setErrorMessage('Dataset name %s is invalid.');

        $datasetName = $helper->ask($input, $output, $question);
        $format = $input->getOption('format');
        $url = $input->getOption('url');
        $datasetId = $ids[$datasetName];

        $output->writeln(
            '<info>Setting the dataset </info>'.$datasetName.'<info> url in</info> '. $format.'<info> format</info>'
        );

        try {
            $this->commandBus()->dispatch(
                new AddDatasetUrlCommand($datasetId, $format, $url)
            );

            $output->writeln(
                '<info>The dataset </info>"'.$datasetName.'"<info> url has been configured.</info>'
            );
        } catch (NotFoundException $e) {
            $output->writeln('<error>'.$e->getMessage().'</error>');
        } catch (ValidationException $e) {
            $this->printValidationErrors($e, $output);
        }
    }

    /**
     * @return Dataset[]
     */
    private function findAllDatasets()
    {
        return $this->queryBus()->dispatch(new FindAllDatasets());
    }
}
