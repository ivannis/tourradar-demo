<?php

/**
 * This file is part of the Tourradar application.
 *
 * Copyright (c) Tourradar
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace AppBundle\Command\Dataset;

use AppBundle\Command\Core\Command;
use Cubiche\Core\Async\Loop\Timer\Timer;
use Cubiche\Core\Bus\Exception\NotFoundException;
use Cubiche\Core\Validator\Exception\ValidationException;
use Cubiche\Core\Async\Loop\Loop;
use Cubiche\Core\Async\Loop\LoopInterface;
use Cubiche\Core\Async\Promise\Promises;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Tourradar\Resource\Domain\Command\DownloadDatasetRequestCommand;
use Tourradar\Resource\Domain\ReadModel\Dataset;
use Tourradar\Resource\Domain\ReadModel\Query\FindAllDatasets;

/**
 * DownloadAllCommand class.
 *
 * @author Ivan Suarez Jerez <ivannis.suarez@gmail.com>
 */
class DownloadAllCommand extends Command
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('app:dataset-download-all')
            ->addOption('format', '', InputOption::VALUE_REQUIRED, 'The download format', 'json')
            ->addOption('output', '', InputOption::VALUE_REQUIRED, 'The download output directory', '/var/www')
            ->addOption('interval', '', InputOption::VALUE_REQUIRED, 'The number of seconds to wait before execution', 1)
            ->addOption('count', '', InputOption::VALUE_REQUIRED, 'The count of downloads', 1)
            ->setDescription('Download all the datasets and convert its into a csv file.')
        ;
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $format = $input->getOption('format');
        $directory = $input->getOption('output');
        $interval = intval($input->getOption('interval'));
        $count = intval($input->getOption('count'));
        $datasets = $this->findAllDatasets();

        $loop = new Loop();
        foreach ($datasets as $dataset) {
            $datasetId   = $dataset->datasetId()->toNative();
            $datasetName = $dataset->name()->toNative();

            $deferred = Promises::defer();
            $loop->timer(
                function(Timer $timer) use ($deferred, $datasetId, $datasetName, $format, $directory, $output, $count) {
                    $output->writeln(
                        sprintf(
                            '%s  ITERATION : %d %s',
                            '<info>----------------------------</info>',
                            $timer->iterations(),
                            '<info>----------------------------</info>'
                        )
                    );

                    $output->writeln(
                        '<info>Downloading the dataset </info>'.$datasetName.'<info> in</info> '. $format.
                        '<info> format</info>'
                    );

                    try {
                        $this->commandBus()->dispatch(
                            new DownloadDatasetRequestCommand($datasetId, $format, $directory)
                        );
                    } catch (NotFoundException $e) {
                        $output->writeln('<error>'.$e->getMessage().'</error>');
                    } catch (ValidationException $e) {
                        $this->printValidationErrors($e, $output);
                    }

                    $output->writeln(
                        '<info>The dataset </info>"'.$datasetName.'"<info> download has been finished.</info>'
                    );

                    if (($count - 1) == $timer->iterations()) {
                        $deferred->resolve();
                    }
                },
                $interval,
                $count
            );

            $promises[] = $deferred->promise();
        }

        Promises::all($promises)->then(function () use ($output){
            $output->writeln(
                '<comment>The datasets downloads has been finished.</comment>'
            );
        });

        $loop->run();
    }

    /**
     * @return Dataset[]
     */
    private function findAllDatasets()
    {
        return $this->queryBus()->dispatch(new FindAllDatasets());
    }
}
