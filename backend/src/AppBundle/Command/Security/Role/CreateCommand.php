<?php

/**
 * This file is part of the Tourradar application.
 *
 * Copyright (c) Tourradar
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace AppBundle\Command\Security\Role;

use AppBundle\Command\Core\Command;
use Cubiche\Core\Validator\Exception\ValidationException;
use Tourradar\Security\Domain\Role\Command\CreateRoleCommand;
use Tourradar\Security\Domain\Role\RoleId;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * CreateCommand class.
 *
 * @author Ivan Suarez Jerez <ivannis.suarez@gmail.com>
 */
class CreateCommand extends Command
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('app:role-create')
            ->addArgument('name', InputArgument::REQUIRED, 'Role name.')
            ->addArgument('permissions', InputArgument::IS_ARRAY | InputArgument::REQUIRED, 'The role permissions.')
            ->setDescription('Creates a new role.')
            ->setHelp('This command allows you to create a role...')
        ;
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            $output->writeln('<info>Creating a new role</info>');

            $roleId = RoleId::next()->toNative();
            $roleName = $input->getArgument('name');
            $permissions = $input->getArgument('permissions');

            $this->commandBus()->dispatch(
                new CreateRoleCommand($roleId, $roleName, $permissions)
            );

            $output->writeln(
                '<info>A new role with name </info>"'.$roleName.
                '"<info> has been successfully created.</info>'
            );
        } catch (ValidationException $e) {
            $this->printValidationErrors($e, $output);
        }
    }
}
