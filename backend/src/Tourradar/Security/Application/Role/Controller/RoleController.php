<?php

/**
 * This file is part of the Tourradar application.
 *
 * Copyright (c) Tourradar
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tourradar\Security\Application\Role\Controller;

use Tourradar\Core\Application\Controller\CommandController;
use Tourradar\Security\Domain\Role\Command\AddPermissionToRoleCommand;
use Tourradar\Security\Domain\Role\Command\CreateRoleCommand;
use Tourradar\Security\Domain\Role\Command\RemovePermissionFromRoleCommand;
use Tourradar\Security\Domain\Role\RoleId;

/**
 * RoleController class.
 *
 * @author Ivan Suarez Jerez <ivannis.suarez@gmail.com>
 */
class RoleController extends CommandController
{
    /**
     * @param string $name
     * @param array  $permissions
     *
     * @return string
     */
    public function createAction($name, array $permissions = array())
    {
        $roleId = RoleId::next()->toNative();

        $this->commandBus()->dispatch(
            new CreateRoleCommand($roleId, $name, $permissions)
        );

        return $roleId;
    }

    /**
     * @param string $roleId
     * @param string $permission
     *
     * @return bool
     */
    public function addPermissionAction($roleId, $permission)
    {
        $this->commandBus()->dispatch(
            new AddPermissionToRoleCommand($roleId, $permission)
        );

        return true;
    }

    /**
     * @param string $roleId
     * @param string $permission
     *
     * @return bool
     */
    public function removePermissionAction($roleId, $permission)
    {
        $this->commandBus()->dispatch(
            new RemovePermissionFromRoleCommand($roleId, $permission)
        );

        return true;
    }
}
