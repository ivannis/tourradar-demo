<?php

/**
 * This file is part of the Tourradar application.
 *
 * Copyright (c) Tourradar
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tourradar\Security\Infrastructure\Role\GraphQL\Query;

use Tourradar\Security\Application\Role\ReadModel\Controller\RoleController;
use Tourradar\Security\Domain\Role\ReadModel\Role;
use Tourradar\Security\Infrastructure\Role\RoleType;
use Youshido\GraphQL\Execution\ResolveInfo;
use Youshido\GraphQL\Field\AbstractField;
use Youshido\GraphQL\Type\ListType\ListType;

/**
 * FindAllRoles class.
 *
 * @author Ivan Suarez Jerez <ivannis.suarez@gmail.com>
 */
class FindAllRoles extends AbstractField
{
    /**
     * @param null        $value
     * @param array       $args
     * @param ResolveInfo $info
     *
     * @return Role[]
     */
    public function resolve($value, array $args, ResolveInfo $info)
    {
        /** @var RoleController $controller */
        $controller = $info->getContainer()->get('app.controller.read_model.role');

        return $controller->findAllAction();
    }

    /**
     * {@inheritdoc}
     */
    public function getType()
    {
        return new ListType(new RoleType());
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'roles';
    }
}
