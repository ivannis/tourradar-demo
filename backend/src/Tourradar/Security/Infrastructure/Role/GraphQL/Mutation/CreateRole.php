<?php

/**
 * This file is part of the Tourradar application.
 *
 * Copyright (c) Tourradar
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tourradar\Security\Infrastructure\Role\GraphQL\Mutation;

use Tourradar\Security\Application\Role\Controller\RoleController;
use Tourradar\Security\Application\Role\ReadModel\Controller\RoleController as ReadRoleController;
use Tourradar\Security\Domain\Role\ReadModel\Role;
use Tourradar\Security\Infrastructure\Role\RoleType;
use Youshido\GraphQL\Config\Field\FieldConfig;
use Youshido\GraphQL\Execution\ResolveInfo;
use Youshido\GraphQL\Field\AbstractField;
use Youshido\GraphQL\Type\ListType\ListType;
use Youshido\GraphQL\Type\NonNullType;
use Youshido\GraphQL\Type\Scalar\StringType;

/**
 * CreateRole class.
 *
 * @author Ivan Suarez Jerez <ivannis.suarez@gmail.com>
 */
class CreateRole extends AbstractField
{
    /**
     * {@inheritdoc}
     */
    public function build(FieldConfig $config)
    {
        $config
            ->addArgument('name', new NonNullType(new StringType()))
            ->addArgument('permissions', new ListType(new StringType()))
        ;
    }

    /**
     * @param null        $value
     * @param array       $args
     * @param ResolveInfo $info
     *
     * @return Role
     */
    public function resolve($value, array $args, ResolveInfo $info)
    {
        /** @var RoleController $controller */
        $controller = $info->getContainer()->get('app.controller.role');

        $roleId = $controller->createAction($args['name'], $args['permissions']);

        /** @var ReadRoleController $controller */
        $controller = $info->getContainer()->get('app.controller.read_model.role');

        return $controller->findOneByIdAction($roleId);
    }

    /**
     * {@inheritdoc}
     */
    public function getType()
    {
        return new RoleType();
    }
}
