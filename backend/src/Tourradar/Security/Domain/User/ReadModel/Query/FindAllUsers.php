<?php

/**
 * This file is part of the Tourradar application.
 *
 * Copyright (c) Tourradar
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tourradar\Security\Domain\User\ReadModel\Query;

use Cubiche\Core\Cqrs\Query\Query;

/**
 * FindAllUsers class.
 *
 * @author Ivan Suarez Jerez <ivannis.suarez@gmail.com>
 */
class FindAllUsers extends Query
{
}
