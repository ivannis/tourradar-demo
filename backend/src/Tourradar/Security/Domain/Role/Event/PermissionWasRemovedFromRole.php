<?php

/**
 * This file is part of the Tourradar application.
 *
 * Copyright (c) Tourradar
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tourradar\Security\Domain\Role\Event;

use Tourradar\Security\Domain\Role\RoleId;
use Cubiche\Domain\EventSourcing\DomainEvent;
use Cubiche\Domain\System\StringLiteral;

/**
 * PermissionWasRemovedFromRole class.
 *
 * @author Ivan Suarez Jerez <ivannis.suarez@gmail.com>
 */
class PermissionWasRemovedFromRole extends DomainEvent
{
    /**
     * @var StringLiteral
     */
    protected $permission;

    /**
     * PermissionWasRemovedFromRole constructor.
     *
     * @param RoleId        $roleId
     * @param StringLiteral $permission
     */
    public function __construct(RoleId $roleId, StringLiteral $permission)
    {
        parent::__construct($roleId);

        $this->permission = $permission;
    }

    /**
     * @return RoleId
     */
    public function roleId()
    {
        return $this->aggregateId();
    }

    /**
     * @return StringLiteral
     */
    public function permission()
    {
        return $this->permission;
    }
}
