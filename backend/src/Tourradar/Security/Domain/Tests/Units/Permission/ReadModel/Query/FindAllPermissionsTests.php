<?php

/**
 * This file is part of the Tourradar application.
 *
 * Copyright (c) Tourradar
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tourradar\Security\Domain\Tests\Units\Permission\ReadModel\Query;

use Tourradar\Core\Domain\Tests\Units\ReadModel\Query\QueryTestTrait;
use Tourradar\Security\Domain\Tests\Units\TestCase;

/**
 * FindAllPermissionsTests class.
 *
 * @author Ivan Suarez Jerez <ivannis.suarez@gmail.com>
 */
class FindAllPermissionsTests extends TestCase
{
    use QueryTestTrait;
}
