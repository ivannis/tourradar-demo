<?php

/**
 * This file is part of the Tourradar application.
 *
 * Copyright (c) Tourradar
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tourradar\Security\Domain\Tests\Units\Role\Event;

use Tourradar\Core\Domain\Tests\Units\Event\EventTestTrait;
use Tourradar\Security\Domain\Role\RoleId;
use Tourradar\Security\Domain\Tests\Units\TestCase;
use Cubiche\Domain\System\StringLiteral;

/**
 * PermissionWasRemovedFromRoleTests class.
 *
 * @author Ivan Suarez Jerez <ivannis.suarez@gmail.com>
 */
class PermissionWasRemovedFromRoleTests extends TestCase
{
    use EventTestTrait;

    /**
     * {@inheritdoc}
     */
    public function getArguments()
    {
        return [
            RoleId::next(),
            StringLiteral::fromNative('app.user.create'),
        ];
    }
}
