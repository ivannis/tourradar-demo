<?php

/**
 * This file is part of the Tourradar application.
 *
 * Copyright (c) Tourradar
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tourradar\Security\Domain\Tests\Units\Role\ReadModel\Query;

use Tourradar\Core\Domain\Tests\Units\ReadModel\Query\QueryTestTrait;
use Tourradar\Security\Domain\Tests\Units\TestCase;

/**
 * FindAllRolesTests class.
 *
 * @author Ivan Suarez Jerez <ivannis.suarez@gmail.com>
 */
class FindAllRolesTests extends TestCase
{
    use QueryTestTrait;
}
