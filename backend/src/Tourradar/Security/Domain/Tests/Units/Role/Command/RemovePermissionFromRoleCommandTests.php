<?php

/**
 * This file is part of the Tourradar application.
 *
 * Copyright (c) Tourradar
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tourradar\Security\Domain\Tests\Units\Role\Command;

use Tourradar\Core\Domain\Tests\Units\Command\CommandTestTrait;
use Tourradar\Security\Domain\Role\RoleId;
use Tourradar\Security\Domain\Tests\Units\TestCase;

/**
 * RemovePermissionFromRoleCommandTests class.
 *
 * @author Ivan Suarez Jerez <ivannis.suarez@gmail.com>
 */
class RemovePermissionFromRoleCommandTests extends TestCase
{
    use CommandTestTrait;

    /**
     * {@inheritdoc}
     */
    public function getArguments()
    {
        return [
            RoleId::next()->toNative(),
            'app.user.create',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function validatorProvider()
    {
        return [
            [
                'assert' => false,
                'arguments' => [
                    $this->faker->word(),
                    'app.user.create',
                ],
            ],
        ];
    }
}
