<?php

/**
 * This file is part of the Tourradar application.
 *
 * Copyright (c) Tourradar
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tourradar\Security\Domain\Tests\Units;

use Tourradar\Core\Domain\Tests\Units\SettingCommandBusTrait;
use Tourradar\Core\Domain\Tests\Units\SettingEventBusTrait;
use Tourradar\Core\Domain\Tests\Units\SettingProjectorTrait;
use Tourradar\Core\Domain\Tests\Units\SettingQueryRepositoryTrait;
use Tourradar\Core\Domain\Tests\Units\SettingWriteRepositoryTrait;
use Tourradar\Core\Domain\Tests\Units\TestCase as BaseTestCase;
use Tourradar\Security\Domain\Permission\ReadModel\PermissionQueryHandler;
use Tourradar\Security\Domain\Permission\ReadModel\Query\FindAllPermissions;
use Tourradar\Security\Domain\Permission\ReadModel\Query\FindOnePermissionByName;
use Tourradar\Security\Domain\Role\Command\AddPermissionToRoleCommand;
use Tourradar\Security\Domain\Role\Command\CreateRoleCommand;
use Tourradar\Security\Domain\Role\Command\RemovePermissionFromRoleCommand;
use Tourradar\Security\Domain\Role\ReadModel\Projection\RoleProjector;
use Tourradar\Security\Domain\Role\ReadModel\Query\FindAllRoles;
use Tourradar\Security\Domain\Role\ReadModel\Query\FindOneRoleById;
use Tourradar\Security\Domain\Role\ReadModel\Query\FindOneRoleByName;
use Tourradar\Security\Domain\Role\ReadModel\Role as ReadModelRole;
use Tourradar\Security\Domain\Role\ReadModel\RoleQueryHandler;
use Tourradar\Security\Domain\Role\Role;
use Tourradar\Security\Domain\Role\RoleCommandHandler;
use Tourradar\Security\Domain\Role\Validator\Asserter as RoleAsserter;
use Tourradar\Security\Domain\User\Command\AddUserRoleCommand;
use Tourradar\Security\Domain\User\Command\CreateUserCommand;
use Tourradar\Security\Domain\User\Command\DisableUserCommand;
use Tourradar\Security\Domain\User\Command\EnableUserCommand;
use Tourradar\Security\Domain\User\Command\LoginUserCommand;
use Tourradar\Security\Domain\User\Command\LogoutUserCommand;
use Tourradar\Security\Domain\User\Command\RemoveUserRoleCommand;
use Tourradar\Security\Domain\User\Command\ResetUserPasswordCommand;
use Tourradar\Security\Domain\User\Command\ResetUserPasswordRequestCommand;
use Tourradar\Security\Domain\User\Command\VerifyUserCommand;
use Tourradar\Security\Domain\User\ReadModel\Projection\UserProjector;
use Tourradar\Security\Domain\User\ReadModel\Query\FindAllUsers;
use Tourradar\Security\Domain\User\ReadModel\Query\FindOneUserByEmail;
use Tourradar\Security\Domain\User\ReadModel\Query\FindOneUserByEmailVerificationToken;
use Tourradar\Security\Domain\User\ReadModel\Query\FindOneUserById;
use Tourradar\Security\Domain\User\ReadModel\Query\FindOneUserByPasswordResetToken;
use Tourradar\Security\Domain\User\ReadModel\Query\FindOneUserByUsername;
use Tourradar\Security\Domain\User\ReadModel\User as ReadModelUser;
use Tourradar\Security\Domain\User\ReadModel\UserQueryHandler;
use Tourradar\Security\Domain\User\Service\Canonicalizer;
use Tourradar\Security\Domain\User\Service\PasswordEncoder;
use Tourradar\Security\Domain\User\Service\SaltGenerator;
use Tourradar\Security\Domain\User\Service\TokenGenerator;
use Tourradar\Security\Domain\User\Service\UserFactory;
use Tourradar\Security\Domain\User\User;
use Tourradar\Security\Domain\User\UserCommandHandler;
use Tourradar\Security\Domain\User\Validator\Asserter as UserAsserter;
use Cubiche\Core\Validator\Validator;
use Cubiche\Domain\EventPublisher\DomainEventPublisher;

/**
 * TestCase class.
 *
 * @author Ivan Suarez Jerez <ivannis.suarez@gmail.com>
 */
class TestCase extends BaseTestCase
{
    use SettingCommandBusTrait;
    use SettingWriteRepositoryTrait;
    use SettingQueryRepositoryTrait;
    use SettingPermissionRepositoryTrait;
    use SettingEventBusTrait, SettingProjectorTrait {
        SettingEventBusTrait::eventDispatcher insteadof SettingProjectorTrait;
    }

    /**
     * {@inheritdoc}
     */
    public function beforeTestMethod($method)
    {
        DomainEventPublisher::set($this->eventBus());
        $this->registerValidatorAsserters();
    }

    /**
     * {@inheritdoc}
     */
    protected function registerValidatorAsserters()
    {
        parent::registerValidatorAsserters();

        $securityContextMock = $this->newMockInstance('Tourradar\Security\Domain\User\Service\SecurityContextInterface');

        $roleAsserter = new RoleAsserter($this->queryBus(), $securityContextMock);
        $userAsserter = new UserAsserter($this->queryBus());

        Validator::registerValidator('uniqueRoleName', array($roleAsserter, 'uniqueRoleName'));
        Validator::registerValidator('uniqueUsername', array($userAsserter, 'uniqueUsername'));
        Validator::registerValidator('uniqueEmail', array($userAsserter, 'uniqueEmail'));
    }

    /**
     * @return array
     */
    protected function commandHandlers()
    {
        $roleCommandHandler = new RoleCommandHandler(
            $this->writeRepository(Role::class),
            $this->permissionRepository()
        );

        $userCommandHandler = new UserCommandHandler(
            $this->writeRepository(User::class),
            new UserFactory(new Canonicalizer(), new PasswordEncoder(), new SaltGenerator()),
            new TokenGenerator(),
            $this->queryBus()
        );

        return [
            CreateRoleCommand::class => $roleCommandHandler,
            AddPermissionToRoleCommand::class => $roleCommandHandler,
            RemovePermissionFromRoleCommand::class => $roleCommandHandler,
            CreateUserCommand::class => $userCommandHandler,
            AddUserRoleCommand::class => $userCommandHandler,
            RemoveUserRoleCommand::class => $userCommandHandler,
            ResetUserPasswordCommand::class => $userCommandHandler,
            ResetUserPasswordRequestCommand::class => $userCommandHandler,
            VerifyUserCommand::class => $userCommandHandler,
            DisableUserCommand::class => $userCommandHandler,
            EnableUserCommand::class => $userCommandHandler,
            LoginUserCommand::class => $userCommandHandler,
            LogoutUserCommand::class => $userCommandHandler,
        ];
    }

    /**
     * @return array
     */
    protected function commandValidatorHandlers()
    {
        return array();
    }

    /**
     * @return array
     */
    protected function queryHandlers()
    {
        $roleQueryHandler = new RoleQueryHandler(
            $this->queryRepository(ReadModelRole::class)
        );

        $permissionQueryHandler = new PermissionQueryHandler(
            $this->permissionRepository()
        );

        $userQueryHandler = new UserQueryHandler(
            $this->queryRepository(ReadModelUser::class),
            new Canonicalizer()
        );

        return [
            FindAllRoles::class => $roleQueryHandler,
            FindOneRoleById::class => $roleQueryHandler,
            FindOneRoleByName::class => $roleQueryHandler,
            FindAllPermissions::class => $permissionQueryHandler,
            FindOnePermissionByName::class => $permissionQueryHandler,
            FindAllUsers::class => $userQueryHandler,
            FindOneUserByEmail::class => $userQueryHandler,
            FindOneUserByUsername::class => $userQueryHandler,
            FindOneUserByEmailVerificationToken::class => $userQueryHandler,
            FindOneUserByPasswordResetToken::class => $userQueryHandler,
            FindOneUserById::class => $userQueryHandler,
        ];
    }

    /**
     * @return array
     */
    protected function queryValidatorHandlers()
    {
        return array();
    }

    /**
     * @return array
     */
    protected function eventSubscribers()
    {
        $roleRepository = $this->queryRepository(ReadModelRole::class);

        $roleProjector = new RoleProjector(
            $roleRepository,
            $this->permissionRepository()
        );

        $userProjector = new UserProjector(
            $this->queryRepository(ReadModelUser::class),
            $roleRepository
        );

        return [
            $roleProjector,
            $userProjector,
        ];
    }
}
