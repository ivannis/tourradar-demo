<?php

/**
 * This file is part of the Tourradar application.
 *
 * Copyright (c) Tourradar
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tourradar\Security\Domain\Permission\ReadModel;

use Cubiche\Core\Specification\Criteria;
use Cubiche\Domain\Repository\QueryRepositoryInterface;
use Tourradar\Security\Domain\Permission\ReadModel\Query\FindAllPermissions;
use Tourradar\Security\Domain\Permission\ReadModel\Query\FindOnePermissionByName;

/**
 * PermissionQueryHandler class.
 *
 * @author Ivan Suarez Jerez <ivannis.suarez@gmail.com>
 */
class PermissionQueryHandler
{
    /**
     * @var QueryRepositoryInterface
     */
    protected $repository;

    /**
     * PermissionQueryHandler constructor.
     *
     * @param QueryRepositoryInterface $repository
     */
    public function __construct(QueryRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param FindAllPermissions $query
     *
     * @return string[]
     */
    public function findAllPermissions(FindAllPermissions $query)
    {
        return $this->repository->getIterator();
    }

    /**
     * @param FindOnePermissionByName $query
     *
     * @return string
     */
    public function findOnePermissionByName(FindOnePermissionByName $query)
    {
        return $this->repository->findOne(
            Criteria::this()->eq($query->name())
        );
    }
}
