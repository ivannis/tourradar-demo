<?php

/**
 * This file is part of the Tourradar application.
 *
 * Copyright (c) Tourradar
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tourradar\Core\Application\Controller;

use Tourradar\Core\Application\Service\TokenContextInterface;
use Tourradar\Core\Domain\Exception\AccessDeniedException;

/**
 * UserAware trait.
 *
 * @author Ivan Suarez Jerez <ivannis.suarez@gmail.com>
 */
trait UserAwareTrait
{
    /**
     * @var TokenContextInterface
     */
    protected $tokenContext;

    /**
     * @param TokenContextInterface $tokenContext
     */
    public function setTokenContext(TokenContextInterface $tokenContext)
    {
        $this->tokenContext = $tokenContext;
    }

    /**
     * @return string
     */
    protected function findCurrentUserOr401()
    {
        if ($this->tokenContext->hasToken()) {
            $token = $this->tokenContext->getToken();

            return $token->userId();
        }

        throw new AccessDeniedException('Protected resource');
    }
}
