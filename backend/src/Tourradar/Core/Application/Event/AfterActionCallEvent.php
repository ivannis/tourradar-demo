<?php

/**
 * This file is part of the Tourradar application.
 *
 * Copyright (c) Tourradar
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tourradar\Core\Application\Event;

use Cubiche\Domain\EventPublisher\DomainEvent;

/**
 * AfterActionCallEvent class.
 *
 * @author Ivan Suarez Jerez <ivannis.suarez@gmail.com>
 */
class AfterActionCallEvent extends DomainEvent
{
    /**
     * @var \ReflectionMethod
     */
    protected $method;

    /**
     * @var array
     */
    protected $arguments;

    /**
     * @var array
     */
    protected $permissions;

    /**
     * AfterActionCallEvent constructor.
     *
     * @param \ReflectionMethod $method
     * @param array             $arguments
     * @param array             $permissions
     */
    public function __construct(\ReflectionMethod $method, array $arguments, array $permissions)
    {
        $this->method = $method;
        $this->arguments = $arguments;
        $this->permissions = $permissions;
    }

    /**
     * @return \ReflectionMethod
     */
    public function method()
    {
        return $this->method;
    }

    /**
     * @return array
     */
    public function arguments()
    {
        return $this->arguments;
    }

    /**
     * @return array
     */
    public function permissions()
    {
        return $this->permissions;
    }
}
