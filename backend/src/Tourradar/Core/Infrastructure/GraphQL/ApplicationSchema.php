<?php

/**
 * This file is part of the Tourradar application.
 *
 * Copyright (c) Tourradar
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tourradar\Core\Infrastructure\GraphQL;

use Cubiche\Core\Validator\Exception\ValidationException;
use Tourradar\Resource\Infrastructure\GraphQL\Mutation\CreateDataset;
use Tourradar\Resource\Infrastructure\GraphQL\Query\FindAllDatasets;
use Tourradar\Resource\Infrastructure\GraphQL\Query\FindAllLogs;
use Tourradar\Resource\Infrastructure\GraphQL\Query\FindAllLogsByDatasetId;
use Tourradar\Resource\Infrastructure\GraphQL\Query\FindAllStats;
use Tourradar\Resource\Infrastructure\GraphQL\Query\FindOneDatasetById;
use Tourradar\Security\Infrastructure\Permission\GraphQL\Query\FindAllPermissions;
use Tourradar\Security\Infrastructure\Role\GraphQL\Mutation\AddPermissionsToRole;
use Tourradar\Security\Infrastructure\Role\GraphQL\Mutation\CreateRole;
use Tourradar\Security\Infrastructure\Role\GraphQL\Mutation\RemovePermissionsFromRole;
use Tourradar\Security\Infrastructure\Role\GraphQL\Query\FindAllRoles;
use Tourradar\Security\Infrastructure\Role\GraphQL\Query\FindOneRoleById;
use Tourradar\Security\Infrastructure\Role\GraphQL\Query\FindOneRoleByName;
use Tourradar\Security\Infrastructure\User\GraphQL\Mutation\CreateUser;
use Tourradar\Security\Infrastructure\User\GraphQL\Mutation\DisableUser;
use Tourradar\Security\Infrastructure\User\GraphQL\Mutation\EnableUser;
use Tourradar\Security\Infrastructure\User\GraphQL\Mutation\LoginUser;
use Tourradar\Security\Infrastructure\User\GraphQL\Mutation\LogoutUser;
use Tourradar\Security\Infrastructure\User\GraphQL\Mutation\ResetUserPasswordRequest;
use Tourradar\Security\Infrastructure\User\GraphQL\Mutation\VerifyUser;
use Tourradar\Security\Infrastructure\User\GraphQL\Query\FindAllUsers;
use Tourradar\Security\Infrastructure\User\GraphQL\Query\FindAuthenticatedUser;
use Tourradar\Security\Infrastructure\User\GraphQL\Query\FindOneUserByPasswordResetToken;
use Tourradar\System\Infrastructure\Language\GraphQL\Query\FindAllLanguages;
use Tourradar\System\Infrastructure\Language\GraphQL\Query\FindOneLanguage;
use Youshido\GraphQL\Config\Schema\SchemaConfig;
use Youshido\GraphQL\Exception\Interfaces\LocationableExceptionInterface;
use Youshido\GraphQL\Schema\AbstractSchema;

/**
 * ApplicationSchema class.
 *
 * @author Ivan Suarez Jerez <ivannis.suarez@gmail.com>
 */
class ApplicationSchema extends AbstractSchema
{
    /**
     * {@inheritdoc}
     */
    public function build(SchemaConfig $config)
    {
        $config->getQuery()->addFields([
            new FindAllLanguages(),
            new FindOneLanguage(),
            new FindAllRoles(),
            new FindOneRoleById(),
            new FindOneRoleByName(),
            new FindAllPermissions(),
            new FindAllUsers(),
            new FindAuthenticatedUser(),
            new FindOneUserByPasswordResetToken(),
            new FindAllDatasets(),
            new FindOneDatasetById(),
            new FindAllLogs(),
            new FindAllLogsByDatasetId(),
            new FindAllStats()
        ]);

        $config->getMutation()->addFields([
            new CreateUser(),
            new VerifyUser(),
            new DisableUser(),
            new EnableUser(),
            new LoginUser(),
            new LogoutUser(),
            new ResetUserPasswordRequest(),
            new CreateRole(),
            new AddPermissionsToRole(),
            new RemovePermissionsFromRole(),
        ]);
    }

    /**
     * @param array $errors
     *
     * @return array
     */
    public static function formatErrors(array $errors)
    {
        $result = [];
        foreach ($errors as $error) {
            if ($error instanceof ValidationException) {
                $validationErrors = array(
                    'message' => $error->getMessage(),
                    'code' => $error->getCode(),
                    'errors' => array(),
                );

                foreach ($error->getErrorExceptions() as $errorException) {
                    $validationErrors['errors'][] = array(
                        'message' => $errorException->getMessage(),
                        'code' => $errorException->getCode(),
                        'path' => $errorException->getPropertyPath(),
                    );
                }

                $result[] = $validationErrors;
            } elseif ($error instanceof LocationableExceptionInterface) {
                $result[] = array_merge(
                    ['message' => $error->getMessage()],
                    $error->getLocation() ? ['locations' => [$error->getLocation()->toArray()]] : [],
                    $error->getCode() ? ['code' => $error->getCode()] : []
                );
            } else {
                $result[] = array_merge(
                    ['message' => $error->getMessage()],
                    $error->getCode() ? ['code' => $error->getCode()] : []
                );
            }
        }

        return $result;
    }
}
