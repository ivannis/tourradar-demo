<?php

/**
 * This file is part of the Tourradar application.
 *
 * Copyright (c) Tourradar
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tourradar\System\Domain\Tests\Units\Language\Event;

use Tourradar\Core\Domain\Tests\Units\Event\EventTestTrait;
use Tourradar\System\Domain\Language\LanguageId;
use Tourradar\System\Domain\Tests\Units\TestCase;
use Cubiche\Domain\Locale\LanguageCode;
use Cubiche\Domain\Localizable\LocalizableString;

/**
 * LanguageWasCreatedTests class.
 *
 * Generated by TestGenerator on 2018-01-25 at 17:26:00.
 */
class LanguageWasCreatedTests extends TestCase
{
    use EventTestTrait;

    /**
     * {@inheritdoc}
     */
    protected function getArguments()
    {
        return array(
            LanguageId::next(),
            LocalizableString::fromArray(array('en_US' => 'English', 'es_ES' => 'Ingles')),
            LanguageCode::EN(),
        );
    }
}
