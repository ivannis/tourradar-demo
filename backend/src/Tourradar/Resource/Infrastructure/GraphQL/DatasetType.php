<?php

/**
 * This file is part of the Tourradar application.
 *
 * Copyright (c) Tourradar
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tourradar\Resource\Infrastructure\GraphQL;

use Tourradar\Resource\Domain\ReadModel\Dataset;
use Youshido\GraphQL\Type\ListType\ListType;
use Youshido\GraphQL\Type\NonNullType;
use Youshido\GraphQL\Type\Object\AbstractObjectType;
use Youshido\GraphQL\Type\Scalar\IdType;
use Youshido\GraphQL\Type\Scalar\IntType;
use Youshido\GraphQL\Type\Scalar\StringType;

/**
 * DatasetType class.
 *
 * @author Ivan Suarez Jerez <ivannis.suarez@gmail.com>
 */
class DatasetType extends AbstractObjectType
{
    /**
     * {@inheritdoc}
     */
    public function build($config)
    {
        $config
            ->addField('id', [
                'type' => new NonNullType(new IdType()),
                'resolve' => function (Dataset $dataset) {
                    return $dataset->id()->toNative();
                },
            ])
            ->addField('name', [
                'type' => new NonNullType(new StringType()),
                'resolve' => function (Dataset $dataset) {
                    return $dataset->name()->toNative();
                },
            ])
            ->addField('urls', [
                'type' => new UrlListType(),
                'resolve' => function (Dataset $dataset) {
                    return array_map(function($format, $url) {
                        return array(
                            'format' => $format,
                            'url' => $url
                        );
                    }, $dataset->urls()->keys()->toArray(), $dataset->urls()->values()->toArray());
                },
            ])
            ->addField('downloads', [
                'type' => new ListType(new DownloadType()),
                'resolve' => function (Dataset $dataset) {
                    return array_map(function ($key, $value) use (&$urls) {
                        return array_merge(
                            $value,
                            array('downloadedAt' => $key)
                        );
                    }, $dataset->downloads()->keys()->toArray(), $dataset->downloads()->values()->toArray());
                },
            ])
            ->addField('downloadsStats', [
                'type' => new NonNullType(new DownloadStatsType()),
                'resolve' => function (Dataset $dataset) {
                    return array(
                        'count' => $dataset->downloads()->count(),
                        'failed' => $dataset->failedDownloads()->count(),
                        'successful' => $dataset->successfulDownloads()->count()
                    );
                },
            ])
            ->addField('createdAt', [
                'type' => new NonNullType(new IntType()),
                'resolve' => function (Dataset $dataset) {
                    return $dataset->createdAt()->toNative()->getTimestamp();
                },
            ])
            ->addField('updatedAt', [
                'type' => new IntType(),
                'resolve' => function (Dataset $dataset) {
                    return $dataset->updatedAt() ? $dataset->updatedAt()->toNative()->getTimestamp() : null;
                },
            ])
        ;
    }
}
