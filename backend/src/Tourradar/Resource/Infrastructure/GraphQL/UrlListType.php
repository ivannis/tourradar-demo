<?php

/**
 * This file is part of the Tourradar application.
 *
 * Copyright (c) Tourradar
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tourradar\Resource\Infrastructure\GraphQL;

use Youshido\GraphQL\Type\ListType\AbstractListType;

/**
 * UrlListType class.
 *
 * @author Ivan Suarez Jerez <ivannis.suarez@gmail.com>
 */
class UrlListType extends AbstractListType
{
    /**
     * {@inheritdoc}
     */
    public function getItemType()
    {
        return new UrlType();
    }
}
