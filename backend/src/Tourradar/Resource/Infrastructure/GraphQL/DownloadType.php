<?php

/**
 * This file is part of the Tourradar application.
 *
 * Copyright (c) Tourradar
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tourradar\Resource\Infrastructure\GraphQL;

use Youshido\GraphQL\Type\NonNullType;
use Youshido\GraphQL\Type\Object\AbstractObjectType;
use Youshido\GraphQL\Type\Scalar\IntType;
use Youshido\GraphQL\Type\Scalar\StringType;

/**
 * DownloadType class.
 *
 * @author Ivan Suarez Jerez <ivannis.suarez@gmail.com>
 */
class DownloadType extends AbstractObjectType
{
    /**
     * {@inheritdoc}
     */
    public function build($config)
    {
        $config
            ->addField('downloadedAt', [
                'type' => new NonNullType(new IntType()),
                'resolve' => function (array $data) {
                    return $data['downloadedAt'];
                },
            ])
            ->addField('format', [
                'type' => new NonNullType(new StringType()),
                'resolve' => function ($data) {
                    return $data['format'];
                },
            ])
            ->addField('state', [
                'type' => new NonNullType(new StringType()),
                'resolve' => function ($data) {
                    return $data['state'];
                },
            ])
        ;
    }
}
