<?php

/**
 * This file is part of the Tourradar application.
 *
 * Copyright (c) Tourradar
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tourradar\Resource\Infrastructure\GraphQL\Query;

use Tourradar\Resource\Application\ReadModel\Controller\DatasetController;
use Tourradar\Resource\Domain\ReadModel\Dataset;
use Tourradar\Resource\Infrastructure\GraphQL\DownloadStatsType;
use Youshido\GraphQL\Execution\ResolveInfo;
use Youshido\GraphQL\Field\AbstractField;

/**
 * FindAllStats class.
 *
 * @author Ivan Suarez Jerez <ivannis.suarez@gmail.com>
 */
class FindAllStats extends AbstractField
{
    /**
     * @param null        $value
     * @param array       $args
     * @param ResolveInfo $info
     *
     * @return Dataset[]
     */
    public function resolve($value, array $args, ResolveInfo $info)
    {
        /** @var DatasetController $controller */
        $controller = $info->getContainer()->get('app.controller.read_model.dataset');
        $datasets = $controller->findAllAction();

        $stats = array('count' => 0, 'failed' => 0, 'successful' => 0,);
        foreach ($datasets as $dataset) {
            $stats['count'] += $dataset->downloads()->count();
            $stats['failed'] += $dataset->failedDownloads()->count();
            $stats['successful'] += $dataset->successfulDownloads()->count();
        }

        return $stats;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'stats';
    }

    /**
     * {@inheritdoc}
     */
    public function getType()
    {
        return new DownloadStatsType();
    }
}
