<?php

/**
 * This file is part of the Tourradar application.
 *
 * Copyright (c) Tourradar
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tourradar\Resource\Infrastructure\GraphQL\Query;

use Tourradar\Resource\Domain\ReadModel\Dataset;
use Tourradar\Resource\Infrastructure\GraphQL\DatasetType;
use Tourradar\Resource\Application\ReadModel\Controller\DatasetController;
use Youshido\GraphQL\Config\Field\FieldConfig;
use Youshido\GraphQL\Execution\ResolveInfo;
use Youshido\GraphQL\Field\AbstractField;
use Youshido\GraphQL\Type\NonNullType;
use Youshido\GraphQL\Type\Scalar\IdType;

/**
 * FindOneDatasetById class.
 *
 * @author Ivan Suarez Jerez <ivannis.suarez@gmail.com>
 */
class FindOneDatasetById extends AbstractField
{
    /**
     * {@inheritdoc}
     */
    public function build(FieldConfig $config)
    {
        $config
            ->addArgument('datasetId', new NonNullType(new IdType()))
        ;
    }

    /**
     * @param null        $value
     * @param array       $args
     * @param ResolveInfo $info
     *
     * @return Dataset
     */
    public function resolve($value, array $args, ResolveInfo $info)
    {
        /** @var DatasetController $controller */
        $controller = $info->getContainer()->get('app.controller.read_model.dataset');

        return $controller->findOneByIdAction($args['datasetId']);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'dataset';
    }

    /**
     * {@inheritdoc}
     */
    public function getType()
    {
        return new DatasetType();
    }
}
