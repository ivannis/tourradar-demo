<?php

/**
 * This file is part of the Tourradar application.
 *
 * Copyright (c) Tourradar
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tourradar\Resource\Infrastructure\GraphQL\Query;

use Tourradar\Resource\Application\ReadModel\Controller\LogController;
use Tourradar\Resource\Domain\ReadModel\Dataset;
use Tourradar\Resource\Infrastructure\GraphQL\LogItemType;
use Youshido\GraphQL\Config\Field\FieldConfig;
use Youshido\GraphQL\Execution\ResolveInfo;
use Youshido\GraphQL\Field\AbstractField;
use Youshido\GraphQL\Type\ListType\ListType;
use Youshido\GraphQL\Type\NonNullType;
use Youshido\GraphQL\Type\Scalar\IdType;

/**
 * FindAllLogsByDatasetId class.
 *
 * @author Ivan Suarez Jerez <ivannis.suarez@gmail.com>
 */
class FindAllLogsByDatasetId extends AbstractField
{
    /**
     * {@inheritdoc}
     */
    public function build(FieldConfig $config)
    {
        $config
            ->addArgument('datasetId', new NonNullType(new IdType()))
        ;
    }

    /**
     * @param null        $value
     * @param array       $args
     * @param ResolveInfo $info
     *
     * @return Dataset[]
     */
    public function resolve($value, array $args, ResolveInfo $info)
    {
        /** @var LogController $controller */
        $controller = $info->getContainer()->get('app.controller.read_model.log');

        return $controller->findAllByDatasetIdAction($args['datasetId']);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'logsByDataset';
    }

    /**
     * {@inheritdoc}
     */
    public function getType()
    {
        return new ListType(new LogItemType());
    }
}
