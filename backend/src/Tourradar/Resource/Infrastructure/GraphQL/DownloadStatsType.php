<?php

/**
 * This file is part of the Tourradar application.
 *
 * Copyright (c) Tourradar
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tourradar\Resource\Infrastructure\GraphQL;

use Youshido\GraphQL\Type\NonNullType;
use Youshido\GraphQL\Type\Object\AbstractObjectType;
use Youshido\GraphQL\Type\Scalar\IntType;

/**
 * DownloadStatsType class.
 *
 * @author Ivan Suarez Jerez <ivannis.suarez@gmail.com>
 */
class DownloadStatsType extends AbstractObjectType
{
    /**
     * {@inheritdoc}
     */
    public function build($config)
    {
        $config
            ->addField('count', [
                'type' => new NonNullType(new IntType()),
                'resolve' => function (array $data) {
                    return $data['count'];
                },
            ])
            ->addField('failed', [
                'type' => new NonNullType(new IntType()),
                'resolve' => function ($data) {
                    return $data['failed'];
                },
            ])
            ->addField('successful', [
                'type' => new NonNullType(new IntType()),
                'resolve' => function ($data) {
                    return $data['successful'];
                },
            ])
        ;
    }
}
