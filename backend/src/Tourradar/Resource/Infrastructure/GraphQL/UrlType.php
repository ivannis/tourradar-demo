<?php

/**
 * This file is part of the Tourradar application.
 *
 * Copyright (c) Tourradar
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tourradar\Resource\Infrastructure\GraphQL;

use Youshido\GraphQL\Type\NonNullType;
use Youshido\GraphQL\Type\Object\AbstractObjectType;
use Youshido\GraphQL\Type\Scalar\StringType;

/**
 * UrlType class.
 *
 * @author Ivan Suarez Jerez <ivannis.suarez@gmail.com>
 */
class UrlType extends AbstractObjectType
{
    /**
     * {@inheritdoc}
     */
    public function build($config)
    {
        $config
            ->addField('format', [
                'type' => new NonNullType(new StringType()),
                'resolve' => function (array $data) {
                    return $data['format'];
                },
            ])
            ->addField('url', [
                'type' => new NonNullType(new StringType()),
                'resolve' => function ($data) {
                    return $data['url'];
                },
            ])
        ;
    }
}
