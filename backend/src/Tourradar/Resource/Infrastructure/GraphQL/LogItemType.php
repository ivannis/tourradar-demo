<?php

/**
 * This file is part of the Tourradar application.
 *
 * Copyright (c) Tourradar
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tourradar\Resource\Infrastructure\GraphQL;

use Tourradar\Resource\Application\ReadModel\Controller\DatasetController;
use Tourradar\Resource\Application\ReadModel\LogItem;
use Youshido\GraphQL\Execution\ResolveInfo;
use Youshido\GraphQL\Type\NonNullType;
use Youshido\GraphQL\Type\Object\AbstractObjectType;
use Youshido\GraphQL\Type\Scalar\IdType;
use Youshido\GraphQL\Type\Scalar\IntType;
use Youshido\GraphQL\Type\Scalar\StringType;

/**
 * LogItemType class.
 *
 * @author Ivan Suarez Jerez <ivannis.suarez@gmail.com>
 */
class LogItemType extends AbstractObjectType
{
    /**
     * {@inheritdoc}
     */
    public function build($config)
    {
        $config
            ->addField('id', [
                'type' => new NonNullType(new IdType()),
                'resolve' => function (LogItem $logItem) {
                    return $logItem->id()->toNative();
                },
            ])
            ->addField('dataset', [
                'type' => new NonNullType(new DatasetType()),
                'resolve' => function (LogItem $logItem, array $args, ResolveInfo $info) {
                    /** @var DatasetController $controller */
                    $controller = $info->getContainer()->get('app.controller.read_model.dataset');

                    return $controller->findOneByIdAction($logItem->datasetId()->toNative());
                },
            ])
            ->addField('level', [
                'type' => new NonNullType(new StringType()),
                'resolve' => function (LogItem $logItem) {
                    return $logItem->level()->toNative();
                },
            ])
            ->addField('message', [
                'type' => new NonNullType(new StringType()),
                'resolve' => function (LogItem $logItem) {
                    return $logItem->message()->toNative();
                },
            ])
            ->addField('createdAt', [
                'type' => new NonNullType(new IntType()),
                'resolve' => function (LogItem $logItem) {
                    return $logItem->createdAt()->toNative()->getTimestamp();
                },
            ])
        ;
    }
}
