<?php

/**
 * This file is part of the Tourradar application.
 *
 * Copyright (c) Tourradar
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tourradar\Resource\Infrastructure\Service;

use Cubiche\Domain\System\StringLiteral;
use Tourradar\Resource\Application\Service\NotificatorInterface;
use Tourradar\System\Domain\Mailer\Service\MailerInterface;

/**
 * EmailNotificator class.
 *
 * @author Ivan Suarez Jerez <ivannis.suarez@gmail.com>
 */
class EmailNotificator implements NotificatorInterface
{
    /**
     * @var MailerInterface
     */
    protected $mailer;

    /**
     * @var string
     */
    protected $notificationEmail;

    /**
     * EmailNotificator constructor.
     *
     * @param MailerInterface $mailer
     */
    public function __construct(MailerInterface $mailer, $notificationEmail)
    {
        $this->mailer = $mailer;
        $this->notificationEmail = $notificationEmail;
    }

    /**
     * {@inheritdoc}
     */
    public function notify($message, array $context = array())
    {
        $this->mailer->send(
            StringLiteral::fromNative('dataset_download_failed'),
            array($this->notificationEmail),
            array(
                'message' => $message,
                'context' => $context,
            )
        );
    }
}
