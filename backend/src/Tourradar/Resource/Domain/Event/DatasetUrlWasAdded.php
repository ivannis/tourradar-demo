<?php

/**
 * This file is part of the Tourradar application.
 *
 * Copyright (c) Tourradar
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tourradar\Resource\Domain\Event;

use Cubiche\Domain\EventSourcing\DomainEvent;
use Cubiche\Domain\Web\Url;
use Tourradar\Resource\Domain\DatasetFormat;
use Tourradar\Resource\Domain\DatasetId;

/**
 * DatasetUrlWasAdded class.
 *
 * @author Ivan Suarez Jerez <ivannis.suarez@gmail.com>
 */
class DatasetUrlWasAdded extends DomainEvent
{
    /**
     * @var DatasetFormat
     */
    protected $format;

    /**
     * @var Url
     */
    protected $url;

    /**
     * DatasetUrlWasAdded constructor.
     *
     * @param DatasetId     $datasetId
     * @param DatasetFormat $format
     * @param Url           $url
     */
    public function __construct(DatasetId $datasetId, DatasetFormat $format, Url $url)
    {
        parent::__construct($datasetId);

        $this->format = $format;
        $this->url = $url;
    }

    /**
     * @return DatasetId
     */
    public function datasetId()
    {
        return $this->aggregateId();
    }

    /**
     * @return DatasetFormat
     */
    public function format()
    {
        return $this->format;
    }

    /**
     * @return Url
     */
    public function url()
    {
        return $this->url;
    }
}
