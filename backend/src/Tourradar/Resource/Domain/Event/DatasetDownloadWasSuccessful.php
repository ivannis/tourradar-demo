<?php

/**
 * This file is part of the Tourradar application.
 *
 * Copyright (c) Tourradar
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tourradar\Resource\Domain\Event;

use Cubiche\Domain\EventSourcing\DomainEvent;
use Tourradar\Resource\Domain\DatasetFormat;
use Tourradar\Resource\Domain\DatasetId;

/**
 * DatasetDownloadWasSuccessful class.
 *
 * @author Ivan Suarez Jerez <ivannis.suarez@gmail.com>
 */
class DatasetDownloadWasSuccessful extends DomainEvent
{
    /**
     * @var DatasetFormat
     */
    protected $format;

    /**
     * DatasetDownloadWasSuccessful constructor.
     *
     * @param DatasetId     $datasetId
     * @param DatasetFormat $format
     */
    public function __construct(DatasetId $datasetId, DatasetFormat $format)
    {
        parent::__construct($datasetId);

        $this->format = $format;
    }

    /**
     * @return DatasetId
     */
    public function datasetId()
    {
        return $this->aggregateId();
    }

    /**
     * @return DatasetFormat
     */
    public function format()
    {
        return $this->format;
    }
}
