<?php

/**
 * This file is part of the Tourradar application.
 *
 * Copyright (c) Tourradar
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tourradar\Resource\Domain\Event;

use Cubiche\Domain\EventSourcing\DomainEvent;
use Cubiche\Domain\System\DateTime\DateTime;
use Cubiche\Domain\System\StringLiteral;
use Tourradar\Resource\Domain\DatasetId;

/**
 * DatasetWasCreated class.
 *
 * @author Ivan Suarez Jerez <ivannis.suarez@gmail.com>
 */
class DatasetWasCreated extends DomainEvent
{
    /**
     * @var StringLiteral
     */
    protected $name;

    /**
     * @var array
     */
    protected $urls;

    /**
     * @var DateTime
     */
    protected $createdAt;

    /**
     * DatasetWasCreated constructor.
     *
     * @param DatasetId    $datasetId
     * @param StringLiteral $name
     * @param array         $urls
     * @param DateTime      $createdAt
     */
    public function __construct( DatasetId $datasetId, StringLiteral $name, array $urls, DateTime $createdAt)
    {
        parent::__construct($datasetId);

        $this->name = $name;
        $this->urls = $urls;
        $this->createdAt = $createdAt;
    }

    /**
     * @return DatasetId
     */
    public function datasetId()
    {
        return $this->aggregateId();
    }

    /**
     * @return StringLiteral
     */
    public function name()
    {
        return $this->name;
    }

    /**
     * @return array
     */
    public function urls()
    {
        return $this->urls;
    }

    /**
     * @return DateTime
     */
    public function createdAt()
    {
        return $this->createdAt;
    }
}
