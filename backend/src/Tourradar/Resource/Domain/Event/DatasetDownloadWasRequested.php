<?php

/**
 * This file is part of the Tourradar application.
 *
 * Copyright (c) Tourradar
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tourradar\Resource\Domain\Event;

use Cubiche\Domain\EventSourcing\DomainEvent;
use Cubiche\Domain\System\DateTime\DateTime;
use Cubiche\Domain\System\StringLiteral;
use Tourradar\Resource\Domain\DatasetFormat;
use Tourradar\Resource\Domain\DatasetId;

/**
 * DatasetDownloadWasRequested class.
 *
 * @author Ivan Suarez Jerez <ivannis.suarez@gmail.com>
 */
class DatasetDownloadWasRequested extends DomainEvent
{
    /**
     * @var DatasetFormat
     */
    protected $format;

    /**
     * @var StringLiteral
     */
    protected $output;

    /**
     * @var DateTime
     */
    protected $downloadRequestedAt;

    /**
     * DatasetDownloadWasRequested constructor.
     *
     * @param DatasetId     $datasetId
     * @param DatasetFormat $format
     * @param StringLiteral $output
     * @param DateTime      $downloadRequestedAt
     */
    public function __construct(
        DatasetId $datasetId,
        DatasetFormat $format,
        StringLiteral $output,
        DateTime $downloadRequestedAt
    ) {
        parent::__construct($datasetId);

        $this->format = $format;
        $this->output = $output;
        $this->downloadRequestedAt = $downloadRequestedAt;
    }

    /**
     * @return DatasetId
     */
    public function datasetId()
    {
        return $this->aggregateId();
    }

    /**
     * @return DatasetFormat
     */
    public function format()
    {
        return $this->format;
    }

    /**
     * @return StringLiteral
     */
    public function output()
    {
        return $this->output;
    }

    /**
     * @return DateTime
     */
    public function downloadRequestedAt()
    {
        return $this->downloadRequestedAt;
    }
}
