<?php

/**
 * This file is part of the Tourradar application.
 *
 * Copyright (c) Tourradar
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tourradar\Resource\Domain\Event;

use Cubiche\Domain\EventSourcing\DomainEvent;
use Cubiche\Domain\System\Integer;
use Cubiche\Domain\System\StringLiteral;
use Tourradar\Resource\Domain\DatasetFormat;
use Tourradar\Resource\Domain\DatasetId;

/**
 * DatasetDownloadFailed class.
 *
 * @author Ivan Suarez Jerez <ivannis.suarez@gmail.com>
 */
class DatasetDownloadFailed extends DomainEvent
{
    /**
     * @var DatasetFormat
     */
    protected $format;

    /**
     * @var StringLiteral
     */
    protected $reason;

    /**
     * @var \Cubiche\Domain\System\Integer
     */
    protected $errorCode;

    /**
     * DatasetDownloadFailed constructor.
     *
     * @param DatasetId     $datasetId
     * @param DatasetFormat $format
     * @param StringLiteral $reason
     * @param Integer       $errorCode
     */
    public function __construct(DatasetId $datasetId, DatasetFormat $format, StringLiteral $reason, Integer $errorCode)
    {
        parent::__construct($datasetId);

        $this->format = $format;
        $this->reason = $reason;
        $this->errorCode = $errorCode;
    }

    /**
     * @return DatasetId
     */
    public function datasetId()
    {
        return $this->aggregateId();
    }

    /**
     * @return DatasetFormat
     */
    public function format()
    {
        return $this->format;
    }

    /**
     * @return StringLiteral
     */
    public function reason()
    {
        return $this->reason;
    }

    /**
     * @return \Cubiche\Domain\System\Integer
     */
    public function errorCode()
    {
        return $this->errorCode;
    }
}
