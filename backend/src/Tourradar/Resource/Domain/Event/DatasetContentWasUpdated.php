<?php

/**
 * This file is part of the Tourradar application.
 *
 * Copyright (c) Tourradar
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tourradar\Resource\Domain\Event;

use Cubiche\Domain\EventSourcing\DomainEvent;
use Cubiche\Domain\System\DateTime\DateTime;
use Cubiche\Domain\System\StringLiteral;
use Tourradar\Resource\Domain\DatasetId;

/**
 * DatasetContentWasUpdated class.
 *
 * @author Ivan Suarez Jerez <ivannis.suarez@gmail.com>
 */
class DatasetContentWasUpdated extends DomainEvent
{
    /**
     * @var StringLiteral
     */
    protected $contentHash;

    /**
     * @var DateTime
     */
    protected $updatedAt;

    /**
     * DatasetContentWasUpdated constructor.
     *
     * @param DatasetId     $datasetId
     * @param StringLiteral $contentHash
     * @param DateTime      $updatedAt
     */
    public function __construct(DatasetId $datasetId, StringLiteral $contentHash, DateTime $updatedAt)
    {
        parent::__construct($datasetId);

        $this->contentHash = $contentHash;
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return DatasetId
     */
    public function datasetId()
    {
        return $this->aggregateId();
    }

    /**
     * @return StringLiteral
     */
    public function contentHash()
    {
        return $this->contentHash;
    }

    /**
     * @return DateTime
     */
    public function updatedAt()
    {
        return $this->updatedAt;
    }
}
