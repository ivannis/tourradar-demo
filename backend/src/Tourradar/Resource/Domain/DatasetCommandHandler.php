<?php

/**
 * This file is part of the Tourradar application.
 *
 * Copyright (c) Tourradar
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tourradar\Resource\Domain;

use Cubiche\Domain\Repository\RepositoryInterface;
use Cubiche\Domain\System\Integer;
use Cubiche\Domain\System\StringLiteral;
use Cubiche\Domain\Web\Url;
use Tourradar\Core\Domain\Exception\NotFoundException;
use Tourradar\Resource\Domain\Command\AddDatasetUrlCommand;
use Tourradar\Resource\Domain\Command\CreateDatasetCommand;
use Tourradar\Resource\Domain\Command\DownloadDatasetRequestCommand;
use Tourradar\Resource\Domain\Command\MarkDatasetAsDownloadedCommand;
use Tourradar\Resource\Domain\Command\RejectDatasetDownloadCommand;
use Tourradar\Resource\Domain\Command\UpdateDatasetContentCommand;
use Tourradar\Resource\Domain\Command\UpdateDatasetUrlCommand;

/**
 * DatasetCommandHandler class.
 *
 * @author Ivan Suarez Jerez <ivannis.suarez@gmail.com>
 */
class DatasetCommandHandler
{
    /**
     * @var RepositoryInterface
     */
    protected $repository;

    /**
     * DatasetCommandHandler constructor.
     *
     * @param RepositoryInterface $repository
     */
    public function __construct(RepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param CreateDatasetCommand $command
     */
    public function createDataset(CreateDatasetCommand $command)
    {
        $urls = array();
        array_map(function ($format, $url) use (&$urls) {
            $urls[$format] = Url::fromNative($url);
        }, array_keys($command->urls()), $command->urls());

        $dataset = new Dataset(
            DatasetId::fromNative($command->datasetId()),
            StringLiteral::fromNative($command->name()),
            $urls
        );

        $this->repository->persist($dataset);
    }

    /**
     * @param UpdateDatasetContentCommand $command
     */
    public function updateDatasetContent(UpdateDatasetContentCommand $command)
    {
        $dataset = $this->findOr404($command->datasetId());

        $dataset->updateContentHash(
            StringLiteral::fromNative($command->contentHash())
        );

        $this->repository->persist($dataset);
    }

    /**
     * @param AddDatasetUrlCommand $command
     */
    public function addDatasetUrl(AddDatasetUrlCommand $command)
    {
        $dataset = $this->findOr404($command->datasetId());

        $dataset->addUrl(
            DatasetFormat::fromNative($command->format()),
            Url::fromNative($command->url())
        );

        $this->repository->persist($dataset);
    }

    /**
     * @param UpdateDatasetUrlCommand $command
     */
    public function updateDatasetUrl(UpdateDatasetUrlCommand $command)
    {
        $dataset = $this->findOr404($command->datasetId());

        $dataset->updateUrl(
            DatasetFormat::fromNative($command->format()),
            Url::fromNative($command->url())
        );

        $this->repository->persist($dataset);
    }

    /**
     * @param DownloadDatasetRequestCommand $command
     */
    public function downloadDatasetRequest(DownloadDatasetRequestCommand $command)
    {
        $dataset = $this->findOr404($command->datasetId());

        $dataset->downloadRequest(
            DatasetFormat::fromNative($command->format()),
            StringLiteral::fromNative($command->output())
        );

        $this->repository->persist($dataset);
    }

    /**
     * @param MarkDatasetAsDownloadedCommand $command
     */
    public function markDatasetAsDownloaded(MarkDatasetAsDownloadedCommand $command)
    {
        $dataset = $this->findOr404($command->datasetId());

        $dataset->downloadSuccessful(
            DatasetFormat::fromNative($command->format())
        );

        $this->repository->persist($dataset);
    }

    /**
     * @param RejectDatasetDownloadCommand $command
     */
    public function rejectDatasetDownload(RejectDatasetDownloadCommand $command)
    {
        $dataset = $this->findOr404($command->datasetId());

        $dataset->downloadFailed(
            DatasetFormat::fromNative($command->format()),
            StringLiteral::fromNative($command->reason()),
            Integer::fromNative($command->errorCode())
        );

        $this->repository->persist($dataset);
    }

    /**
     * @param string $datasetId
     *
     * @return \Tourradar\Resource\Domain\Dataset
     */
    private function findOr404($datasetId)
    {
        $dataset = $this->repository->get(DatasetId::fromNative($datasetId));
        if ($dataset === null) {
            throw new NotFoundException(sprintf(
                'There is no dataset with id: %s',
                $datasetId
            ));
        }

        return $dataset;
    }
}
