<?php

/**
 * This file is part of the Tourradar application.
 *
 * Copyright (c) Tourradar
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tourradar\Resource\Domain;

use Cubiche\Domain\System\Enum;

/**
 * DatasetFormat class.
 *
 * @method static DatasetFormat JSON()
 * @method static DatasetFormat XML()
 * @method static DatasetFormat CSV()
 * @method static DatasetFormat TSV()
 *
 * @author Ivan Suarez Jerez <ivannis.suarez@gmail.com>
 */
class DatasetFormat extends Enum
{
    const JSON = 'json';
    const XML  = 'xml';
    const CSV  = 'csv';
    const TSV  = 'tsv';
}
