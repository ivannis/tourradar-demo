<?php

/**
 * This file is part of the Tourradar application.
 *
 * Copyright (c) Tourradar
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tourradar\Resource\Domain\Validator;

use Cubiche\Core\Validator\Assert;
use Cubiche\Core\Validator\Exception\InvalidArgumentException;
use Tourradar\Resource\Domain\DatasetFormat;

/**
 * Asserter class.
 *
 * @author Ivan Suarez Jerez <ivannis.suarez@gmail.com>
 */
class Asserter
{
    /**
     * @param mixed                $value
     * @param string|callable|null $message
     * @param string|null          $propertyPath
     *
     * @return bool
     *
     * @throws InvalidArgumentException
     */
    public static function downloadFormat($value, $message = null, $propertyPath = null)
    {
        $message = sprintf(
            Assert::generateMessage($message ?: 'There is no support for the "%s" format.'),
            Assert::stringify($value)
        );

        return Assert::inArray($value, DatasetFormat::toArray(), $message, $propertyPath);
    }
}
