<?php

/**
 * This file is part of the Tourradar application.
 *
 * Copyright (c) Tourradar
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tourradar\Resource\Domain\ReadModel\Projection;

use Cubiche\Domain\EventPublisher\DomainEventSubscriberInterface;
use Cubiche\Domain\Repository\QueryRepositoryInterface;
use Cubiche\Domain\System\DateTime\DateTime;
use Tourradar\Core\Domain\Exception\NotFoundException;
use Tourradar\Resource\Domain\DatasetId;
use Tourradar\Resource\Domain\Event\DatasetContentWasUpdated;
use Tourradar\Resource\Domain\Event\DatasetDownloadFailed;
use Tourradar\Resource\Domain\Event\DatasetDownloadWasSuccessful;
use Tourradar\Resource\Domain\Event\DatasetUrlWasAdded;
use Tourradar\Resource\Domain\Event\DatasetUrlWasUpdated;
use Tourradar\Resource\Domain\Event\DatasetWasCreated;
use Tourradar\Resource\Domain\ReadModel\Dataset;

/**
 * DatasetProjector class.
 *
 * @author Ivan Suarez Jerez <ivannis.suarez@gmail.com>
 */
class DatasetProjector implements DomainEventSubscriberInterface
{
    /**
     * @var QueryRepositoryInterface
     */
    protected $repository;

    /**
     * DatasetProjector constructor.
     *
     * @param QueryRepositoryInterface $repository
     */
    public function __construct(QueryRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param DatasetWasCreated $event
     */
    public function whenDatasetWasCreated(DatasetWasCreated $event)
    {
        $readModel = new Dataset(
            $event->datasetId(),
            $event->name(),
            $event->urls(),
            $event->createdAt()
        );

        $this->repository->persist($readModel);
    }

    /**
     * @param DatasetContentWasUpdated $event
     */
    public function whenDatasetContentWasUpdated(DatasetContentWasUpdated $event)
    {
        $readModel = $this->findOr404($event->datasetId());

        $readModel->setContentHash($event->contentHash());
        $readModel->setUpdatedAt($event->updatedAt());

        $this->repository->persist($readModel);
    }

    /**
     * @param DatasetUrlWasAdded $event
     */
    public function whenDatasetUrlWasAdded(DatasetUrlWasAdded $event)
    {
        $readModel = $this->findOr404($event->datasetId());
        $readModel->setUrl($event->format(), $event->url());

        $this->repository->persist($readModel);
    }

    /**
     * @param DatasetUrlWasUpdated $event
     */
    public function whenDatasetUrlWasUpdated(DatasetUrlWasUpdated $event)
    {
        $readModel = $this->findOr404($event->datasetId());
        $readModel->setUrl($event->format(), $event->url());

        $this->repository->persist($readModel);
    }

    /**
     * @param DatasetDownloadFailed $event
     */
    public function whenDatasetDownloadFailed(DatasetDownloadFailed $event)
    {
        $readModel = $this->findOr404($event->datasetId());

        $readModel->addFailedDownload(
            DateTime::fromNative($event->occurredOn()),
            $event->format()
        );

        $this->repository->persist($readModel);
    }

    /**
     * @param DatasetDownloadWasSuccessful $event
     */
    public function whenDatasetDownloadWasSuccessful(DatasetDownloadWasSuccessful $event)
    {
        $readModel = $this->findOr404($event->datasetId());

        $readModel->addSuccessfulDownload(
            DateTime::fromNative($event->occurredOn()),
            $event->format()
        );

        $this->repository->persist($readModel);
    }

    /**
     * @param DatasetId $datasetId
     *
     * @return \Tourradar\Resource\Domain\ReadModel\Dataset
     */
    private function findOr404(DatasetId $datasetId)
    {
        $dataset = $this->repository->get($datasetId);
        if ($dataset === null) {
            throw new NotFoundException(sprintf(
                'There is no dataset with id: %s',
                $datasetId
            ));
        }

        return $dataset;
    }

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return array(
            DatasetWasCreated::class => array('whenDatasetWasCreated', 250),
            DatasetContentWasUpdated::class => array('whenDatasetContentWasUpdated', 250),
            DatasetUrlWasAdded::class => array('whenDatasetUrlWasAdded', 250),
            DatasetUrlWasUpdated::class => array('whenDatasetUrlWasUpdated', 250),
            DatasetDownloadFailed::class => array('whenDatasetDownloadFailed', 250),
            DatasetDownloadWasSuccessful::class => array('whenDatasetDownloadWasSuccessful', 250),
        );
    }
}
