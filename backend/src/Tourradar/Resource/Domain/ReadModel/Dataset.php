<?php

/**
 * This file is part of the Tourradar application.
 *
 * Copyright (c) Tourradar
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source availableTickets.
 */

namespace Tourradar\Resource\Domain\ReadModel;

use Cubiche\Core\Collections\ArrayCollection\ArrayHashMap;
use Cubiche\Core\Collections\ArrayCollection\SortedArrayHashMap;
use Cubiche\Core\Specification\Criteria;
use Cubiche\Domain\EventSourcing\ReadModelInterface;
use Cubiche\Domain\Model\Entity;
use Cubiche\Domain\System\DateTime\DateTime;
use Cubiche\Domain\System\StringLiteral;
use Cubiche\Domain\Web\Url;
use Tourradar\Resource\Domain\DatasetFormat;
use Tourradar\Resource\Domain\DatasetId;

/**
 * Dataset class.
 *
 * @author Ivan Suarez Jerez <ivannis.suarez@gmail.com>
 */
class Dataset extends Entity implements ReadModelInterface
{
    /**
     * @var StringLiteral
     */
    protected $name;

    /**
     * @var ArrayHashMap|Url[]
     */
    protected $urls;

    /**
     * @var StringLiteral
     */
    protected $contentHash;

    /**
     * @var DateTime
     */
    protected $createdAt;

    /**
     * @var DateTime
     */
    protected $updatedAt;

    /**
     * @var SortedArrayHashMap
     */
    protected $downloads;

    /**
     * Dataset constructor.
     *
     * @param DatasetId     $datasetId
     * @param StringLiteral $name
     * @param array         $urls
     * @param DateTime      $createdAt
     */
    public function __construct(
        DatasetId $datasetId,
        StringLiteral $name,
        array $urls,
        DateTime $createdAt
    ) {
        parent::__construct($datasetId);

        $this->name = $name;
        $this->createdAt = $createdAt;

        $this->urls = new ArrayHashMap();
        $this->downloads = new SortedArrayHashMap();

        foreach ($urls as $format => $url) {
            $this->setUrl(DatasetFormat::fromNative($format), $url);
        }
    }

    /**
     * @return DatasetId
     */
    public function datasetId()
    {
        return $this->id;
    }

    /**
     * @return StringLiteral
     */
    public function name()
    {
        return $this->name;
    }

    /**
     * @return ArrayHashMap|Url[]
     */
    public function urls()
    {
        return $this->urls;
    }

    /**
     * @param DatasetFormat $format
     *
     * @return Url|null
     */
    public function url(DatasetFormat $format)
    {
        return $this->urls->containsKey($format->toNative()) ? $this->urls->get($format->toNative()) : null;
    }

    /**
     * @param DatasetFormat $format
     * @param Url           $url
     */
    public function setUrl(DatasetFormat $format, Url $url)
    {
        $this->urls->set($format->toNative(), $url);
    }

    /**
     * @return StringLiteral
     */
    public function contentHash()
    {
        return $this->contentHash;
    }

    /**
     * @param StringLiteral $contentHash
     */
    public function setContentHash(StringLiteral $contentHash)
    {
        $this->contentHash = $contentHash;
    }

    /**
     * @return DateTime
     */
    public function createdAt()
    {
        return $this->createdAt;
    }

    /**
     * @return DateTime
     */
    public function updatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param DateTime $updatedAt
     */
    public function setUpdatedAt(DateTime $updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return SortedArrayHashMap
     */
    public function downloads()
    {
        return $this->downloads;
    }

    /**
     * @return \Cubiche\Core\Collections\CollectionInterface
     */
    public function successfulDownloads()
    {
        return $this->downloads->find(Criteria::key('state')->eq(DownloadState::SUCCESSFUL));
    }

    /**
     * @return \Cubiche\Core\Collections\CollectionInterface
     */
    public function failedDownloads()
    {
        return $this->downloads->find(Criteria::key('state')->eq(DownloadState::FAILED));
    }

    /**
     * @param DateTime      $createdAt
     * @param DatasetFormat $format
     */
    public function addSuccessfulDownload(DateTime $createdAt, DatasetFormat $format)
    {
        $this->downloads->set(
            $createdAt->toNative()->getTimestamp(),
            array(
                'format' => $format,
                'state' => DownloadState::SUCCESSFUL()
            )
        );
    }

    /**
     * @param DateTime      $createdAt
     * @param DatasetFormat $format
     */
    public function addFailedDownload(DateTime $createdAt, DatasetFormat $format)
    {
        $this->downloads->set(
            $createdAt->toNative()->getTimestamp(),
            array(
                'format' => $format,
                'state' => DownloadState::FAILED()
            )
        );
    }
}
