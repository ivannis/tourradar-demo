<?php

/**
 * This file is part of the Tourradar application.
 *
 * Copyright (c) Tourradar
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tourradar\Resource\Domain\ReadModel;

use Cubiche\Core\Collections\CollectionInterface;
use Cubiche\Core\Specification\Criteria;
use Cubiche\Domain\Repository\QueryRepositoryInterface;
use Tourradar\Resource\Domain\ReadModel\Query\FindAllDatasets;
use Tourradar\Resource\Domain\ReadModel\Query\FindOneDatasetById;
use Tourradar\Resource\Domain\DatasetId;

/**
 * DatasetQueryHandler class.
 *
 * @author Ivan Suarez Jerez <ivannis.suarez@gmail.com>
 */
class DatasetQueryHandler
{
    /**
     * @var QueryRepositoryInterface
     */
    protected $repository;

    /**
     * DatasetQueryHandler constructor.
     *
     * @param QueryRepositoryInterface $repository
     */
    public function __construct(QueryRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param FindAllDatasets $query
     *
     * @return CollectionInterface|Dataset[]
     */
    public function findAllDatasets(FindAllDatasets $query)
    {
        return $this->repository->getIterator();
    }

    /**
     * @param FindOneDatasetById $query
     *
     * @return Dataset
     */
    public function findOneDatasetById(FindOneDatasetById $query)
    {
        return $this->repository->findOne(
            Criteria::property('id')->eq(DatasetId::fromNative($query->datasetId()))
        );
    }
}
