<?php

/** * This file is part of the Tourradar application.
 * Copyright (c) Tourradar.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source datasetId.
 */

namespace Tourradar\Resource\Domain\ReadModel\Query;

use Cubiche\Core\Cqrs\Query\Query;
use Cubiche\Core\Validator\Assertion;
use Cubiche\Core\Validator\Mapping\ClassMetadata;

/**
 * FindOneDatasetById class.
 *
 * @author Ivan Suarez Jerez <ivannis.suarez@gmail.com>
 */
class FindOneDatasetById extends Query
{
    /**
     * @var string
     */
    protected $datasetId;

    /**
     * FindOneDatasetById constructor.
     *
     * @param string $datasetId
     */
    public function __construct($datasetId)
    {
        $this->datasetId = $datasetId;
    }

    /**
     * @return string
     */
    public function datasetId()
    {
        return $this->datasetId;
    }

    /**
     * {@inheritdoc}
     */
    public static function loadValidatorMetadata(ClassMetadata $classMetadata)
    {
        $classMetadata->addPropertyConstraint('datasetId', Assertion::uuid()->notBlank());
    }
}
