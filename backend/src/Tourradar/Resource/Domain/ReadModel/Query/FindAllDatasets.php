<?php

/** * This file is part of the Tourradar application.
 * Copyright (c) Tourradar.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tourradar\Resource\Domain\ReadModel\Query;

use Cubiche\Core\Cqrs\Query\Query;

/**
 * FindAllDatasets class.
 *
 * @author Ivan Suarez Jerez <ivannis.suarez@gmail.com>
 */
class FindAllDatasets extends Query
{
}
