<?php

/**
 * This file is part of the Tourradar application.
 *
 * Copyright (c) Tourradar
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tourradar\Resource\Domain\Command;

use Cubiche\Core\Cqrs\Command\Command;
use Cubiche\Core\Validator\Assertion;
use Cubiche\Core\Validator\Mapping\ClassMetadata;

/**
 * CreateDatasetCommand class.
 *
 * @author Ivan Suarez Jerez <ivannis.suarez@gmail.com>
 */
class CreateDatasetCommand extends Command
{
    /**
     * @var string
     */
    protected $datasetId;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var array
     */
    protected $urls;

    /**
     * CreateDatasetCommand constructor.
     *
     * @param string $datasetId
     * @param string $name
     * @param array  $urls
     */
    public function __construct($datasetId, $name, array $urls)
    {
        $this->datasetId = $datasetId;
        $this->name = $name;
        $this->urls = $urls;
    }

    /**
     * @return string
     */
    public function datasetId()
    {
        return $this->datasetId;
    }

    /**
     * @return string
     */
    public function name()
    {
        return $this->name;
    }

    /**
     * @return array
     */
    public function urls()
    {
        return $this->urls;
    }

    /**
     * {@inheritdoc}
     */
    public static function loadValidatorMetadata(ClassMetadata $classMetadata)
    {
        $classMetadata->addPropertyConstraint('datasetId', Assertion::uuid()->notBlank());
        $classMetadata->addPropertyConstraint('name', Assertion::string()->notBlank());

        $classMetadata->addPropertyConstraint(
            'urls',
            Assertion::isArray()->each(Assertion::downloadFormat()->notBlank(), Assertion::url()->notBlank())
        );
    }
}
