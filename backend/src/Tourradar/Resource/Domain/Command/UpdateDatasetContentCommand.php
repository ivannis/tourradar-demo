<?php

/**
 * This file is part of the Tourradar application.
 *
 * Copyright (c) Tourradar
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tourradar\Resource\Domain\Command;

use Cubiche\Core\Cqrs\Command\Command;
use Cubiche\Core\Validator\Assertion;
use Cubiche\Core\Validator\Mapping\ClassMetadata;

/**
 * UpdateDatasetContentCommand class.
 *
 * @author Ivan Suarez Jerez <ivannis.suarez@gmail.com>
 */
class UpdateDatasetContentCommand extends Command
{
    /**
     * @var string
     */
    protected $datasetId;

    /**
     * @var string
     */
    protected $contentHash;

    /**
     * AddDatasetUrlCommand constructor.
     *
     * @param string $datasetId
     * @param string $contentHash
     */
    public function __construct($datasetId, $contentHash)
    {
        $this->datasetId = $datasetId;
        $this->contentHash = $contentHash;
    }


    /**
     * @return string
     */
    public function datasetId()
    {
        return $this->datasetId;
    }

    /**
     * @return string
     */
    public function contentHash()
    {
        return $this->contentHash;
    }

    /**
     * {@inheritdoc}
     */
    public static function loadValidatorMetadata(ClassMetadata $classMetadata)
    {
        $classMetadata->addPropertyConstraint('datasetId', Assertion::uuid()->notBlank());
        $classMetadata->addPropertyConstraint('contentHash', Assertion::string()->notBlank());
    }
}
