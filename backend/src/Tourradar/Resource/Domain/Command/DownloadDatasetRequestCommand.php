<?php

/**
 * This file is part of the Tourradar application.
 *
 * Copyright (c) Tourradar
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tourradar\Resource\Domain\Command;

use Cubiche\Core\Cqrs\Command\Command;
use Cubiche\Core\Validator\Assertion;
use Cubiche\Core\Validator\Mapping\ClassMetadata;

/**
 * DownloadDatasetRequestCommand class.
 *
 * @author Ivan Suarez Jerez <ivannis.suarez@gmail.com>
 */
class DownloadDatasetRequestCommand extends Command
{
    /**
     * @var string
     */
    protected $datasetId;

    /**
     * @var string
     */
    protected $format;

    /**
     * @var string
     */
    protected $output;

    /**
     * AddDatasetUrlCommand constructor.
     *
     * @param string $datasetId
     * @param string $format
     * @param string $output
     */
    public function __construct($datasetId, $format, $output)
    {
        $this->datasetId = $datasetId;
        $this->format = $format;
        $this->output = $output;
    }


    /**
     * @return string
     */
    public function datasetId()
    {
        return $this->datasetId;
    }

    /**
     * @return string
     */
    public function format()
    {
        return $this->format;
    }

    /**
     * @return string
     */
    public function output()
    {
        return $this->output;
    }

    /**
     * {@inheritdoc}
     */
    public static function loadValidatorMetadata(ClassMetadata $classMetadata)
    {
        $classMetadata->addPropertyConstraint('datasetId', Assertion::uuid()->notBlank());
        $classMetadata->addPropertyConstraint('format', Assertion::downloadFormat()->notBlank());
        $classMetadata->addPropertyConstraint('output', Assertion::string()->notBlank());
    }
}
