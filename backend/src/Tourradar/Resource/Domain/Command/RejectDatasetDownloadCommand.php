<?php

/**
 * This file is part of the Tourradar application.
 *
 * Copyright (c) Tourradar
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tourradar\Resource\Domain\Command;

use Cubiche\Core\Cqrs\Command\Command;
use Cubiche\Core\Validator\Assertion;
use Cubiche\Core\Validator\Mapping\ClassMetadata;

/**
 * RejectDatasetDownloadCommand class.
 *
 * @author Ivan Suarez Jerez <ivannis.suarez@gmail.com>
 */
class RejectDatasetDownloadCommand extends Command
{
    /**
     * @var string
     */
    protected $datasetId;

    /**
     * @var string
     */
    protected $format;

    /**
     * @var string
     */
    protected $reason;

    /**
     * @var int
     */
    protected $errorCode;

    /**
     * RejectDatasetDownloadCommand constructor.
     *
     * @param string $datasetId
     * @param string $format
     * @param string $reason
     * @param int    $errorCode
     */
    public function __construct($datasetId, $format, $reason, $errorCode)
    {
        $this->datasetId = $datasetId;
        $this->format = $format;
        $this->reason = $reason;
        $this->errorCode = $errorCode;
    }

    /**
     * @return string
     */
    public function datasetId()
    {
        return $this->datasetId;
    }

    /**
     * @return string
     */
    public function format()
    {
        return $this->format;
    }

    /**
     * @return string
     */
    public function reason()
    {
        return $this->reason;
    }

    /**
     * @return int
     */
    public function errorCode()
    {
        return $this->errorCode;
    }

    /**
     * {@inheritdoc}
     */
    public static function loadValidatorMetadata(ClassMetadata $classMetadata)
    {
        $classMetadata->addPropertyConstraint('datasetId', Assertion::uuid()->notBlank());
        $classMetadata->addPropertyConstraint('format', Assertion::downloadFormat()->notBlank());
        $classMetadata->addPropertyConstraint('reason', Assertion::string()->notBlank());
        $classMetadata->addPropertyConstraint('errorCode', Assertion::integer()->notBlank());
    }
}
