<?php

/**
 * This file is part of the Tourradar application.
 *
 * Copyright (c) Tourradar
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tourradar\Resource\Domain;

use Cubiche\Core\Collections\ArrayCollection\ArrayHashMap;
use Cubiche\Domain\EventSourcing\AggregateRoot;
use Cubiche\Domain\System\DateTime\DateTime;
use Cubiche\Domain\System\Integer;
use Cubiche\Domain\System\StringLiteral;
use Cubiche\Domain\Web\Url;
use Tourradar\Resource\Domain\Event\DatasetContentWasUpdated;
use Tourradar\Resource\Domain\Event\DatasetDownloadFailed;
use Tourradar\Resource\Domain\Event\DatasetDownloadWasRequested;
use Tourradar\Resource\Domain\Event\DatasetDownloadWasSuccessful;
use Tourradar\Resource\Domain\Event\DatasetUrlWasAdded;
use Tourradar\Resource\Domain\Event\DatasetUrlWasUpdated;
use Tourradar\Resource\Domain\Event\DatasetWasCreated;

/**
 * Dataset class.
 *
 * @author Ivan Suarez Jerez <ivannis.suarez@gmail.com>
 */
class Dataset extends AggregateRoot
{
    /**
     * @var StringLiteral
     */
    protected $name;

    /**
     * @var ArrayHashMap|Url[]
     */
    protected $urls;

    /**
     * @var StringLiteral
     */
    protected $contentHash;

    /**
     * @var DateTime
     */
    protected $createdAt;

    /**
     * @var DateTime
     */
    protected $updatedAt;

    /**
     * @var DateTime
     */
    protected $downloadRequestedAt;

    /**
     * Dataset constructor.
     *
     * @param DatasetId     $datasetId
     * @param StringLiteral $name
     * @param array         $urls
     */
    public function __construct(
        DatasetId $datasetId,
        StringLiteral $name,
        array $urls
    ) {
        parent::__construct($datasetId);

        $this->recordAndApplyEvent(
            new DatasetWasCreated(
                $datasetId,
                $name,
                $urls,
                DateTime::now()
            )
        );
    }

    /**
     * @return DatasetId
     */
    public function datasetId()
    {
        return $this->id;
    }

    /**
     * @return StringLiteral
     */
    public function name()
    {
        return $this->name;
    }

    /**
     * @return ArrayHashMap
     */
    public function urls()
    {
        return $this->urls;
    }
    /**
     * @param DatasetFormat $format
     *
     * @return Url|null
     */
    public function url(DatasetFormat $format)
    {
        return $this->urls->containsKey($format->toNative()) ? $this->urls->get($format->toNative()) : null;
    }

    /**
     * @param DatasetFormat $format
     * @param Url           $url
     */
    public function addUrl(DatasetFormat $format, Url $url)
    {
        $this->recordAndApplyEvent(
            new DatasetUrlWasAdded(
                $this->datasetId(),
                $format,
                $url
            )
        );
    }

    /**
     * @param DatasetFormat $format
     * @param Url           $url
     */
    public function updateUrl(DatasetFormat $format, Url $url)
    {
        $this->recordAndApplyEvent(
            new DatasetUrlWasUpdated(
                $this->datasetId(),
                $format,
                $url
            )
        );
    }

    /**
     * @return StringLiteral
     */
    public function contentHash()
    {
        return $this->contentHash;
    }

    /**
     * @param StringLiteral $contentHash
     */
    public function updateContentHash(StringLiteral $contentHash)
    {
        $this->recordAndApplyEvent(
            new DatasetContentWasUpdated(
                $this->datasetId(),
                $contentHash,
                DateTime::now()
            )
        );
    }

    /**
     * @return DateTime
     */
    public function createdAt()
    {
        return $this->createdAt;
    }

    /**
     * @return DateTime
     */
    public function updatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param DatasetFormat $format
     * @param StringLiteral $output
     */
    public function downloadRequest(DatasetFormat $format, StringLiteral $output)
    {
        $this->recordAndApplyEvent(
            new DatasetDownloadWasRequested(
                $this->datasetId(),
                $format,
                $output,
                DateTime::now()
            )
        );
    }

    /**
     * @param DatasetFormat $format
     * @param StringLiteral $reason
     * @param Integer       $errorCode
     */
    public function downloadFailed(DatasetFormat $format, StringLiteral $reason, Integer $errorCode)
    {
        $this->recordAndApplyEvent(
            new DatasetDownloadFailed(
                $this->datasetId(),
                $format,
                $reason,
                $errorCode
            )
        );
    }

    /**
     * @param DatasetFormat $format
     */
    public function downloadSuccessful(DatasetFormat $format)
    {
        $this->recordAndApplyEvent(
            new DatasetDownloadWasSuccessful(
                $this->datasetId(),
                $format
            )
        );
    }

    /**
     * @param DatasetWasCreated $event
     */
    protected function applyDatasetWasCreated(DatasetWasCreated $event)
    {
        $this->name = $event->name();
        $this->createdAt = $event->createdAt();
        $this->urls = new ArrayHashMap();

        /**
         * @var string $format
         * @var Url $url
         */
        foreach ($event->urls() as $format => $url) {
            $this->urls->set($format, $url);
        }
    }

    /**
     * @param DatasetUrlWasAdded $event
     */
    protected function applyDatasetUrlWasAdded(DatasetUrlWasAdded $event)
    {
        $this->urls->set($event->format()->toNative(), $event->url());
    }

    /**
     * @param DatasetUrlWasUpdated $event
     */
    protected function applyDatasetUrlWasUpdated(DatasetUrlWasUpdated $event)
    {
        $this->urls->set($event->format()->toNative(), $event->url());
    }

    /**
     * @param DatasetContentWasUpdated $event
     */
    protected function applyDatasetContentWasUpdated(DatasetContentWasUpdated $event)
    {
        $this->contentHash = $event->contentHash();
        $this->updatedAt = $event->updatedAt();
    }

    /**
     * @param DatasetDownloadWasRequested $event
     */
    protected function applyDatasetDownloadWasRequested(DatasetDownloadWasRequested $event)
    {
        $this->downloadRequestedAt = $event->downloadRequestedAt();
    }

    /**
     * @param DatasetDownloadFailed $event
     */
    protected function applyDatasetDownloadFailed(DatasetDownloadFailed $event)
    {
        $this->downloadRequestedAt = null;
    }

    /**
     * @param DatasetDownloadWasSuccessful $event
     */
    protected function applyDatasetDownloadWasSuccessful(DatasetDownloadWasSuccessful $event)
    {
        $this->downloadRequestedAt = null;
    }
}
