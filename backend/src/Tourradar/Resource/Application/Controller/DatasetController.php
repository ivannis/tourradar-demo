<?php

/**
 * This file is part of the Tourradar application.
 *
 * Copyright (c) Tourradar
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tourradar\Resource\Application\Controller;

use Tourradar\Core\Application\Controller\CommandController;
use Tourradar\Resource\Domain\Command\AddDatasetUrlCommand;
use Tourradar\Resource\Domain\Command\CreateDatasetCommand;
use Tourradar\Resource\Domain\Command\DownloadDatasetRequestCommand;
use Tourradar\Resource\Domain\Command\MarkDatasetAsDownloadedCommand;
use Tourradar\Resource\Domain\Command\RejectDatasetDownloadCommand;
use Tourradar\Resource\Domain\Command\UpdateDatasetContentCommand;
use Tourradar\Resource\Domain\Command\UpdateDatasetUrlCommand;
use Tourradar\Resource\Domain\DatasetId;

/**
 * DatasetController class.
 *
 * @author Ivan Suarez Jerez <ivannis.suarez@gmail.com>
 */
class DatasetController extends CommandController
{
    /**
     * @param string $name
     * @param array  $urls
     *
     * @return string
     */
    public function createAction($name, array $urls)
    {
        $datasetId = DatasetId::next()->toNative();
        $this->commandBus()->dispatch(
            new CreateDatasetCommand($datasetId, $name, $urls)
        );

        return $datasetId;
    }

    /**
     * @param string $datasetId
     * @param string $format
     * @param string $url
     *
     * @return bool
     */
    public function addUrlAction($datasetId, $format, $url)
    {
        $this->commandBus()->dispatch(
            new AddDatasetUrlCommand($datasetId, $format, $url)
        );

        return true;
    }

    /**
     * @param string $datasetId
     * @param string $format
     * @param string $url
     *
     * @return bool
     */
    public function updateUrlAction($datasetId, $format, $url)
    {
        $this->commandBus()->dispatch(
            new UpdateDatasetUrlCommand($datasetId, $format, $url)
        );

        return true;
    }

    /**
     * @param string $datasetId
     * @param string $format
     * @param string $output
     *
     * @return bool
     */
    public function downloadRequestAction($datasetId, $format, $output)
    {
        $this->commandBus()->dispatch(
            new DownloadDatasetRequestCommand($datasetId, $format, $output)
        );

        return true;
    }
}
