<?php

/**
 * This file is part of the Tourradar application.
 *
 * Copyright (c) Tourradar
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tourradar\Resource\Application;

use Cubiche\Core\Cqrs\Command\CommandBus;
use Cubiche\Domain\Repository\RepositoryInterface;
use Cubiche\Domain\System\StringLiteral;
use Tourradar\Core\Domain\Exception\Exception;
use Tourradar\Core\Domain\Exception\NotFoundException;
use Tourradar\Resource\Application\Command\DownloadDatasetCommand;
use Tourradar\Resource\Application\Command\NotifyErrorCommand;
use Tourradar\Resource\Application\Service\DownloaderInterface;
use Tourradar\Resource\Application\Service\GeneratorInterface;
use Tourradar\Resource\Application\Service\NotificatorInterface;
use Tourradar\Resource\Domain\Command\MarkDatasetAsDownloadedCommand;
use Tourradar\Resource\Domain\Command\RejectDatasetDownloadCommand;
use Tourradar\Resource\Domain\Command\UpdateDatasetContentCommand;
use Tourradar\Resource\Domain\Dataset;
use Tourradar\Resource\Domain\DatasetFormat;
use Tourradar\Resource\Domain\DatasetId;
use Cocur\Slugify\Slugify;

/**
 * DownloaderCommandHandler class.
 *
 * @author Ivan Suarez Jerez <ivannis.suarez@gmail.com>
 */
class DownloaderCommandHandler
{
    /**
     * @var NotificatorInterface
     */
    protected $notificator;

    /**
     * @var DownloaderInterface
     */
    protected $downloader;

    /**
     * @var GeneratorInterface
     */
    protected $generator;

    /**
     * @var RepositoryInterface
     */
    protected $repository;

    /**
     * @var CommandBus
     */
    protected $commandBus;

    /**
     * DownloaderCommandHandler constructor.
     *
     * @param NotificatorInterface $notificator
     * @param DownloaderInterface  $downloader
     * @param GeneratorInterface   $generator
     * @param RepositoryInterface  $repository
     * @param CommandBus           $commandBus
     */
    public function __construct(
        NotificatorInterface $notificator,
        DownloaderInterface $downloader,
        GeneratorInterface $generator,
        RepositoryInterface $repository,
        CommandBus $commandBus
    ) {
        $this->notificator = $notificator;
        $this->downloader = $downloader;
        $this->generator = $generator;
        $this->repository = $repository;
        $this->commandBus = $commandBus;
    }

    /**
     * @param DownloadDatasetCommand $command
     */
    public function downloadDataset(DownloadDatasetCommand $command)
    {
        $dataset = $this->findOr404($command->datasetId());
        $url = $dataset->url(DatasetFormat::fromNative($command->format()));

        if ($url === null) {
            $this->downloadFailed(
                $command->datasetId(),
                $command->format(),
                sprintf('There is no url defined for the "%s" format', $command->format()),
                600
            );
        } else {
            try {
                $data = $this->downloader->download($url->toNative(), $command->format());

                $shouldBeGenerated = false;
                $slugify = new Slugify();
                $filename = sprintf(
                    "%s/%s.csv",
                    $command->output(),
                    $slugify->slugify($dataset->name()->toNative())
                );

                $contentHash = StringLiteral::fromNative(base64_encode(json_encode($data)));
                if (!($dataset->contentHash() !== null && $dataset->contentHash()->equals($contentHash))) {
                    // the dataset content changed
                    $this->contentChanged($command->datasetId(), $contentHash->toNative());

                    $shouldBeGenerated = true;
                } elseif (!file_exists($filename)) {
                    $shouldBeGenerated = true;
                }

                // generate the csv file
                if ($shouldBeGenerated) {
                    $this->generator->generate(
                        $data['columns'],
                        $data['rows'],
                        $filename
                    );
                }

                $this->downloadSuccessful($command->datasetId(), $command->format());
            } catch (Exception $e) {
                $this->downloadFailed(
                    $command->datasetId(),
                    $command->format(),
                    $e->getMessage(),
                    $e->getStatusCode()
                );
            }
        }
    }

    /**
     * @param NotifyErrorCommand $command
     */
    public function notifyError(NotifyErrorCommand $command)
    {
        $this->notificator->notify($command->message(), $command->context());
    }

    /**
     * @param string $datasetId
     * @param string $contentHash
     */
    private function contentChanged($datasetId, $contentHash)
    {
        $this->commandBus->dispatch(
            new UpdateDatasetContentCommand($datasetId, $contentHash)
        );
    }

    /**
     * @param string $datasetId
     * @param string $format
     * @param string $message
     * @param int    $errorCode
     */
    private function downloadFailed($datasetId, $format, $message, $errorCode)
    {
        $this->commandBus->dispatch(
            new RejectDatasetDownloadCommand($datasetId, $format, $message, $errorCode)
        );
    }

    /**
     * @param string $datasetId
     * @param string $format
     */
    private function downloadSuccessful($datasetId, $format)
    {
        $this->commandBus->dispatch(
            new MarkDatasetAsDownloadedCommand($datasetId, $format)
        );
    }

    /**
     * @param string $datasetId
     *
     * @return Dataset
     */
    private function findOr404($datasetId)
    {
        /** @var Dataset $dataset */
        $dataset = $this->repository->get(DatasetId::fromNative($datasetId));
        if ($dataset === null) {
            throw new NotFoundException(sprintf(
                'There is no dataset with id: %s',
                $datasetId
            ));
        }

        return $dataset;
    }
}
