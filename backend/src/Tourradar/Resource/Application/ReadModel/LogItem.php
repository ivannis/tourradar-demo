<?php

/**
 * This file is part of the Tourradar application.
 *
 * Copyright (c) Tourradar
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source availableTickets.
 */

namespace Tourradar\Resource\Application\ReadModel;

use Cubiche\Core\Comparable\ComparableInterface;
use Cubiche\Core\Comparable\ComparatorInterface;
use Cubiche\Core\Visitor\VisitorInterface;
use Cubiche\Domain\EventSourcing\ReadModelInterface;
use Cubiche\Domain\Model\Entity;
use Cubiche\Domain\System\DateTime\DateTime;
use Cubiche\Domain\System\StringLiteral;
use Tourradar\Resource\Domain\DatasetId;

/**
 * LogItem class.
 *
 * @author Ivan Suarez Jerez <ivannis.suarez@gmail.com>
 */
class LogItem extends Entity implements ReadModelInterface, ComparableInterface
{
    /**
     * @var DatasetId
     */
    protected $datasetId;

    /**
     * @var LogLevel
     */
    protected $level;

    /**
     * @var StringLiteral
     */
    protected $message;

    /**
     * @var DateTime
     */
    protected $createdAt;

    /**
     * LogItem constructor.
     *
     * @param DatasetId     $datasetId
     * @param LogItemId     $logItemtId
     * @param LogLevel      $level
     * @param StringLiteral $message
     * @param DateTime      $createdAt
     */
    public function __construct(
        DatasetId $datasetId,
        LogItemId $logItemtId,
        LogLevel $level,
        StringLiteral $message,
        DateTime $createdAt
    ) {
        parent::__construct($logItemtId);

        $this->datasetId = $datasetId;
        $this->level = $level;
        $this->message = $message;
        $this->createdAt = $createdAt;
    }

    /**
     * @return LogItemId
     */
    public function logItemtId()
    {
        return $this->id;
    }

    /**
     * @return DatasetId
     */
    public function datasetId()
    {
        return $this->datasetId;
    }

    /**
     * @return LogLevel
     */
    public function level()
    {
        return $this->level;
    }

    /**
     * @return StringLiteral
     */
    public function message()
    {
        return $this->message;
    }

    /**
     * @return DateTime
     */
    public function createdAt()
    {
        return $this->createdAt;
    }

    /**
     * {@inheritdoc}
     */
    public function compareTo($other)
    {
        if ($other instanceof self) {
            $b = $this->createdAt()->timestamp();
            $a = $other->createdAt()->timestamp();

            return $a < $b ? -1 : ($a == $b ? 0 : 1);
        }

        throw new \InvalidArgumentException(sprintf(
            'Argument "%s" is invalid. Allowed types for argument are "%s" or LogItem values.',
            $other,
            self::class
        ));
    }
}
