<?php

/**
 * This file is part of the Tourradar application.
 *
 * Copyright (c) Tourradar
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tourradar\Resource\Application\ReadModel\Controller;

use Cubiche\Core\Collections\CollectionInterface;
use Tourradar\Core\Application\Controller\QueryController;
use Tourradar\Resource\Application\ReadModel\Query\FindAllLogs;
use Tourradar\Resource\Application\ReadModel\Query\FindAllLogsByDatasetId;
use Tourradar\Resource\Domain\ReadModel\Dataset;

/**
 * LogController class.
 *
 * @author Ivan Suarez Jerez <ivannis.suarez@gmail.com>
 */
class LogController extends QueryController
{
    /**
     * @return CollectionInterface|Dataset[]
     */
    public function findAllAction()
    {
        return $this->queryBus()->dispatch(new FindAllLogs());
    }

    /**
     * @param string $datasetId
     *
     * @return Dataset|null
     */
    public function findAllByDatasetIdAction($datasetId)
    {
        return $this->queryBus()->dispatch(new FindAllLogsByDatasetId($datasetId));
    }
}
