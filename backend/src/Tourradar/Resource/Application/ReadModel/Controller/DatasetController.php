<?php

/**
 * This file is part of the Tourradar application.
 *
 * Copyright (c) Tourradar
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tourradar\Resource\Application\ReadModel\Controller;

use Cubiche\Core\Collections\CollectionInterface;
use Tourradar\Core\Application\Controller\QueryController;
use Tourradar\Resource\Domain\ReadModel\Dataset;
use Tourradar\Resource\Domain\ReadModel\Query\FindAllDatasets;
use Tourradar\Resource\Domain\ReadModel\Query\FindOneDatasetById;

/**
 * DatasetController class.
 *
 * @author Ivan Suarez Jerez <ivannis.suarez@gmail.com>
 */
class DatasetController extends QueryController
{
    /**
     * @return CollectionInterface|Dataset[]
     */
    public function findAllAction()
    {
        return $this->queryBus()->dispatch(new FindAllDatasets());
    }

    /**
     * @param string $datasetId
     *
     * @return Dataset|null
     */
    public function findOneByIdAction($datasetId)
    {
        return $this->queryBus()->dispatch(new FindOneDatasetById($datasetId));
    }
}
