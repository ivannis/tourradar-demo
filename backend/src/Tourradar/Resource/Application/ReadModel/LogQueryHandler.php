<?php

/**
 * This file is part of the Tourradar application.
 *
 * Copyright (c) Tourradar
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tourradar\Resource\Application\ReadModel;

use Cubiche\Core\Collections\ArrayCollection\SortedArrayList;
use Cubiche\Core\Collections\CollectionInterface;
use Cubiche\Core\Comparable\Comparator;
use Cubiche\Core\Specification\Criteria;
use Cubiche\Domain\Repository\QueryRepositoryInterface;
use Tourradar\Resource\Application\ReadModel\Query\FindAllLogs;
use Tourradar\Resource\Application\ReadModel\Query\FindAllLogsByDatasetId;
use Tourradar\Resource\Domain\DatasetId;

/**
 * LogQueryHandler class.
 *
 * @author Ivan Suarez Jerez <ivannis.suarez@gmail.com>
 */
class LogQueryHandler
{
    /**
     * @var QueryRepositoryInterface
     */
    protected $repository;

    /**
     * DatasetQueryHandler constructor.
     *
     * @param QueryRepositoryInterface $repository
     */
    public function __construct(QueryRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param FindAllLogs $query
     *
     * @return CollectionInterface|LogItem[]
     */
    public function findAllLogs(FindAllLogs $query)
    {
        return new SortedArrayList($this->repository->toArray());
    }

    /**
     * @param FindAllLogsByDatasetId $query
     *
     * @return CollectionInterface|LogItem[]
     */
    public function findAllLogsByDatasetId(FindAllLogsByDatasetId $query)
    {
        return $this->repository->find(
            Criteria::property('datasetId')->eq(DatasetId::fromNative($query->datasetId()))
        );
    }
}
