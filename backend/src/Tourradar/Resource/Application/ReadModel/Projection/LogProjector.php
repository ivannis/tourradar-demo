<?php

/**
 * This file is part of the Tourradar application.
 *
 * Copyright (c) Tourradar
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tourradar\Resource\Application\ReadModel\Projection;

use Cubiche\Domain\EventPublisher\DomainEventSubscriberInterface;
use Cubiche\Domain\Repository\QueryRepositoryInterface;
use Cubiche\Domain\System\DateTime\DateTime;
use Cubiche\Domain\System\StringLiteral;
use Tourradar\Core\Domain\Exception\NotFoundException;
use Tourradar\Resource\Application\ReadModel\LogItem;
use Tourradar\Resource\Application\ReadModel\LogItemId;
use Tourradar\Resource\Application\ReadModel\LogLevel;
use Tourradar\Resource\Domain\DatasetId;
use Tourradar\Resource\Domain\Event\DatasetContentWasUpdated;
use Tourradar\Resource\Domain\Event\DatasetDownloadFailed;
use Tourradar\Resource\Domain\Event\DatasetDownloadWasSuccessful;
use Tourradar\Resource\Domain\Event\DatasetUrlWasAdded;
use Tourradar\Resource\Domain\Event\DatasetUrlWasUpdated;
use Tourradar\Resource\Domain\Event\DatasetWasCreated;

/**
 * LogProjector class.
 *
 * @author Ivan Suarez Jerez <ivannis.suarez@gmail.com>
 */
class LogProjector implements DomainEventSubscriberInterface
{
    /**
     * @var QueryRepositoryInterface
     */
    protected $repository;

    /**
     * @var QueryRepositoryInterface
     */
    protected $datasetRepository;

    /**
     * DatasetProjector constructor.
     *
     * @param QueryRepositoryInterface $repository
     * @param QueryRepositoryInterface $datasetRepository
     */
    public function __construct(QueryRepositoryInterface $repository, QueryRepositoryInterface $datasetRepository)
    {
        $this->repository = $repository;
        $this->datasetRepository = $datasetRepository;
    }

    /**
     * @param DatasetWasCreated $event
     */
    public function whenDatasetWasCreated(DatasetWasCreated $event)
    {
        $logItemId = LogItemId::next();

        $readModel = new LogItem(
            $event->datasetId(),
            $logItemId,
            LogLevel::INFO(),
            StringLiteral::fromNative(sprintf('The dataset "%s" was created.', $event->name()->toNative())),
            DateTime::fromNative($event->occurredOn())
        );

        $this->repository->persist($readModel);
    }

    /**
     * @param DatasetContentWasUpdated $event
     */
    public function whenDatasetContentWasUpdated(DatasetContentWasUpdated $event)
    {
        $logItemId = LogItemId::next();
        $dataset = $this->findOr404($event->datasetId());

        $readModel = new LogItem(
            $event->datasetId(),
            $logItemId,
            LogLevel::INFO(),
            StringLiteral::fromNative(sprintf('The dataset "%s" was updated.', $dataset->name()->toNative())),
            DateTime::fromNative($event->occurredOn())
        );

        $this->repository->persist($readModel);
    }

    /**
     * @param DatasetUrlWasAdded $event
     */
    public function whenDatasetUrlWasAdded(DatasetUrlWasAdded $event)
    {
        $logItemId = LogItemId::next();
        $dataset = $this->findOr404($event->datasetId());

        $readModel = new LogItem(
            $event->datasetId(),
            $logItemId,
            LogLevel::INFO(),
            StringLiteral::fromNative(
                sprintf(
                    'A new url was added with the "%s" format in the "%s" dataset.',
                    $event->format()->toNative(),
                    $dataset->name()->toNative()
                )
            ),
            DateTime::fromNative($event->occurredOn())
        );

        $this->repository->persist($readModel);
    }

    /**
     * @param DatasetUrlWasUpdated $event
     */
    public function whenDatasetUrlWasUpdated(DatasetUrlWasUpdated $event)
    {
        $logItemId = LogItemId::next();
        $dataset = $this->findOr404($event->datasetId());

        $readModel = new LogItem(
            $event->datasetId(),
            $logItemId,
            LogLevel::INFO(),
            StringLiteral::fromNative(
                sprintf(
                    'The url for the "%s" format in the "%s" dataset was updated.',
                    $event->format()->toNative(),
                    $dataset->name()->toNative()
                )
            ),
            DateTime::fromNative($event->occurredOn())
        );

        $this->repository->persist($readModel);
    }

    /**
     * @param DatasetDownloadFailed $event
     */
    public function whenDatasetDownloadFailed(DatasetDownloadFailed $event)
    {
        $logItemId = LogItemId::next();

        switch ($event->errorCode()->toNative()) {
            case 500: // Internal Server Error
            case 502: // Bad Gateway
            case 503: // Service Unavailable
                $logLevel = LogLevel::EMERGENCY();
                break;
            case 400: // Bad request
            case 401: // Unauthorized
            case 403: // Forbidden
            case 404: // Not found
                $logLevel = LogLevel::ALERT();
                break;
            case 405: // Method Not Allowed
            case 408: // Request Timeout
                $logLevel = LogLevel::CRITICAL();
                break;
            default:
                $logLevel = LogLevel::ERROR();
        }

        $readModel = new LogItem(
            $event->datasetId(),
            $logItemId,
            $logLevel,
            $event->reason(),
            DateTime::fromNative($event->occurredOn())
        );

        $this->repository->persist($readModel);
    }

    /**
     * @param DatasetDownloadWasSuccessful $event
     */
    public function whenDatasetDownloadWasSuccessful(DatasetDownloadWasSuccessful $event)
    {
        $logItemId = LogItemId::next();
        $dataset = $this->findOr404($event->datasetId());

        $readModel = new LogItem(
            $event->datasetId(),
            $logItemId,
            LogLevel::NOTICE(),
            StringLiteral::fromNative(
                sprintf(
                    'The download for the "%s" format in the "%s" dataset was successful.',
                    $event->format()->toNative(),
                    $dataset->name()->toNative()
                )
            ),
            DateTime::fromNative($event->occurredOn())
        );

        $this->repository->persist($readModel);
    }

    /**
     * @param DatasetId $datasetId
     *
     * @return \Tourradar\Resource\Domain\ReadModel\Dataset
     */
    private function findOr404(DatasetId $datasetId)
    {
        $dataset = $this->datasetRepository->get($datasetId);
        if ($dataset === null) {
            throw new NotFoundException(sprintf(
                'There is no dataset with id: %s',
                $datasetId
            ));
        }

        return $dataset;
    }

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return array(
            DatasetWasCreated::class => array('whenDatasetWasCreated', 250),
            DatasetContentWasUpdated::class => array('whenDatasetContentWasUpdated', 250),
            DatasetUrlWasAdded::class => array('whenDatasetUrlWasAdded', 250),
            DatasetUrlWasUpdated::class => array('whenDatasetUrlWasUpdated', 250),
            DatasetDownloadFailed::class => array('whenDatasetDownloadFailed', 250),
            DatasetDownloadWasSuccessful::class => array('whenDatasetDownloadWasSuccessful', 250),
        );
    }
}
