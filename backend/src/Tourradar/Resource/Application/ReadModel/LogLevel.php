<?php

/**
 * This file is part of the Tourradar application.
 *
 * Copyright (c) Tourradar
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tourradar\Resource\Application\ReadModel;

use Cubiche\Domain\System\Enum;

/**
 * LogLevel class.
 *
 * @method static LogLevel EMERGENCY()
 * @method static LogLevel ALERT()
 * @method static LogLevel CRITICAL()
 * @method static LogLevel ERROR()
 * @method static LogLevel WARNING()
 * @method static LogLevel NOTICE()
 * @method static LogLevel INFO()
 *
 * @author Ivan Suarez Jerez <ivannis.suarez@gmail.com>
 */
class LogLevel extends Enum
{
    const EMERGENCY = 'emergency';
    const ALERT     = 'alert';
    const CRITICAL  = 'critical';
    const ERROR     = 'error';
    const WARNING   = 'warning';
    const NOTICE    = 'notice';
    const INFO      = 'info';
}
