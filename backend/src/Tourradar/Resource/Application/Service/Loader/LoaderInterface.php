<?php

/**
 * This file is part of the Tourradar application.
 *
 * Copyright (c) Tourradar
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tourradar\Resource\Application\Service\Loader;

/**
 * LoaderInterface.
 *
 * @author Ivan Suarez Jerez <ivannis.suarez@gmail.com>
 */
interface LoaderInterface
{
    /**
     * Load a dataset url and return the content as an array.
     *
     * @param string $url
     * @param string $format
     *
     * @return array
     */
    public function load($url, $format);

    /**
     * @param string $format
     *
     * @return bool
     */
    public function support($format);
}
