<?php

/**
 * This file is part of the Tourradar application.
 *
 * Copyright (c) Tourradar
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tourradar\Resource\Application\Service\Loader;

use Requests;
use Tourradar\Core\Domain\Exception\Exception;

/**
 * JSONLoader class.
 *
 * @author Ivan Suárez Jerez <ivan@howaboutsales.com>
 */
class JSONLoader implements LoaderInterface
{
    /**
     * {@inheritdoc}
     */
    public function load($url, $format)
    {
        $response = Requests::get($url);
        if ($response->status_code !== 200) {
            throw new Exception(
                $response->status_code,
                sprintf('The url %s trigger the following error: %s.', $url, $response->body)
            );
        } else {
            $data = json_decode($response->body, true);
            if (JSON_ERROR_NONE !== json_last_error()) {
                throw new Exception(
                    603,
                    sprintf('The url %s has an invalid JSON.', $url)
                );
            }

            if ($this->emptyData($data)) {
                // empty data
                throw new Exception(
                    602,
                    sprintf('The url %s was loaded, but seems that there is no content.', $url)
                );
            }

            return $this->filterData($data);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function support($format)
    {
        return strtolower($format) === 'json';
    }

    /**
     * @param array $data
     *
     * @return bool
     */
    private function emptyData(array $data)
    {
        if (empty($data)) {
            return true;
        }

        if (empty($data['meta'])) {
            return true;
        }

        if (empty($data['meta']['view'])) {
            return true;
        }

        if (empty($data['meta']['view']['columns']) || empty($data['data'])) {
            return true;
        }

        return false;
    }

    /**
     * @param array $data
     *
     * @return array
     */
    private function filterData(array $data)
    {
        $columns = array();
        $ignoredIndexs = array();
        foreach ($data['meta']['view']['columns'] as $index => $column) {
            // ignore the hidden columns
            if ($column['id'] !== -1) {
                $columns[] = $column['name'];
            } else {
                $ignoredIndexs[] = $index;
            }
        }

        $rows = array();
        foreach ($data['data'] as $item) {
            $row = array();
            foreach ($item as $index => $value) {
                if (!in_array($index, $ignoredIndexs)) {
                    $row[] = is_array($value) ? $this->filterArray($value) : $value;
                }
            }

            $rows[] = $row;
        }

        return array(
            'columns' => $columns,
            'rows' => $rows,
        );
    }

    /**
     * @param array $data
     *
     * @return string|null
     */
    private function filterArray(array $data)
    {
        $result = array_filter($data, 'strlen');

        return empty($result) ? null : json_encode($result);
    }
}
