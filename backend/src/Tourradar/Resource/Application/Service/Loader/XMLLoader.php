<?php

/**
 * This file is part of the Tourradar application.
 *
 * Copyright (c) Tourradar
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tourradar\Resource\Application\Service\Loader;

use Requests;
use Tourradar\Core\Domain\Exception\Exception;

/**
 * XMLLoader class.
 *
 * @author Ivan Suárez Jerez <ivan@howaboutsales.com>
 */
class XMLLoader implements LoaderInterface
{
    /**
     * {@inheritdoc}
     */
    public function load($url, $format)
    {
        $response = Requests::get($url);
        if ($response->status_code !== 200) {
            throw new Exception(
                $response->status_code,
                sprintf('The url %s trigger the following error: %s.', $url, $response->body)
            );
        } else {
            $xml = simplexml_load_string($response->body);
            if ($xml === false) {
                throw new Exception(
                    603,
                    sprintf('The url %s has an invalid XML.', $url)
                );
            }

            $columns = array();
            $rows = array();
            $firstRow = true;

            foreach ($xml->row->children() as $element) {
                $row = array();
                foreach ($element->children() as $item) {
                    if ($firstRow) {
                        $columns[] = strtoupper(str_replace('_', ' ', $item->getName()));
                    }

                    $row[] = $item->__toString();
                }

                $firstRow = false;
                $rows[] = $row;
            }

            return array(
                'columns' => $columns,
                'rows' => $rows,
            );
        }
    }

    /**
     * {@inheritdoc}
     */
    public function support($format)
    {
        return strtolower($format) === 'xml';
    }
}
