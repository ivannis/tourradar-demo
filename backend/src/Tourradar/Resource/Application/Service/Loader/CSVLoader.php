<?php

/**
 * This file is part of the Tourradar application.
 *
 * Copyright (c) Tourradar
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tourradar\Resource\Application\Service\Loader;

use Tourradar\Core\Domain\Exception\Exception;

/**
 * CSVLoader class.
 *
 * @author Ivan Suárez Jerez <ivan@howaboutsales.com>
 */
class CSVLoader implements LoaderInterface
{
    /**
     * {@inheritdoc}
     */
    public function load($url, $format)
    {
        // empty data
        throw new Exception(
            602,
            sprintf('The url %s was loaded, but seems that there is no content.', $url)
        );

        // TODO:
    }

    /**
     * {@inheritdoc}
     */
    public function support($format)
    {
        return strtolower($format) === 'csv';
    }
}
