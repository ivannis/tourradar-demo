<?php

/**
 * This file is part of the Tourradar application.
 *
 * Copyright (c) Tourradar
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tourradar\Resource\Application\Service\Loader;

use Cubiche\Core\Collections\ArrayCollection\ArrayList;

/**
 * LoaderInterface.
 *
 * @author Ivan Suarez Jerez <ivannis.suarez@gmail.com>
 */
class ChainLoader implements LoaderInterface
{
    /**
     * @var ArrayList
     */
    protected $loaders;

    /**
     * ChainLoader constructor.
     *
     * @param array $loaders
     */
    public function __construct(array $loaders = array())
    {
        $this->loaders = new ArrayList();
        foreach ($loaders as $loader) {
            $this->addLoader($loader);
        }
    }

    /**
     * @param LoaderInterface $loader
     */
    public function addLoader(LoaderInterface $loader)
    {
        $this->loaders->add($loader);
    }

    /**
     * {@inheritdoc}
     */
    public function load($url, $format)
    {
        /** @var $loader LoaderInterface */
        foreach ($this->loaders->toArray() as $loader) {
            if ($loader->support($format)) {
                return $loader->load($url, $format);
            }
        }

        throw new \LogicException('There is no loaders registered.');
    }

    /**
     * {@inheritdoc}
     */
    public function support($format)
    {
        return true;
    }
}
