<?php

/**
 * This file is part of the Tourradar application.
 *
 * Copyright (c) Tourradar
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tourradar\Resource\Application\Service;

/**
 * GeneratorInterface.
 *
 * @author Ivan Suarez Jerez <ivannis.suarez@gmail.com>
 */
interface GeneratorInterface
{
    /**
     * Generate a csv file.
     *
     * @param array  $columns
     * @param array  $rows
     * @param string $output
     * @param string $delimiter
     * @param string $enclosure
     */
    public function generate(array $columns, array $rows, $output, $delimiter = ',', $enclosure = ' ');
}
