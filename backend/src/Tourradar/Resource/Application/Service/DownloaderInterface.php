<?php

/**
 * This file is part of the Tourradar application.
 *
 * Copyright (c) Tourradar
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tourradar\Resource\Application\Service;

/**
 * DownloaderInterface.
 *
 * @author Ivan Suarez Jerez <ivannis.suarez@gmail.com>
 */
interface DownloaderInterface
{
    /**
     * Download a dataset.
     *
     * @param string $url
     * @param string $format
     *
     * @return void
     */
    public function download($url, $format);
}
