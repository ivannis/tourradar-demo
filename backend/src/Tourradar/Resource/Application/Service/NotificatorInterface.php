<?php

/**
 * This file is part of the Tourradar application.
 *
 * Copyright (c) Tourradar
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tourradar\Resource\Application\Service;

/**
 * NotificatorInterface.
 *
 * @author Ivan Suarez Jerez <ivannis.suarez@gmail.com>
 */
interface NotificatorInterface
{
    /**
     * Notify a message.
     *
     * @param string $message
     * @param array  $context
     *
     * @return void
     */
    public function notify($message, array $context = array());
}
