<?php

/**
 * This file is part of the Tourradar application.
 *
 * Copyright (c) Tourradar
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tourradar\Resource\Application\Service;

use Tourradar\Core\Domain\Exception\Exception;
use Tourradar\Resource\Application\Service\Loader\LoaderInterface;

/**
 * Downloader class.
 *
 * @author Ivan Suárez Jerez <ivan@howaboutsales.com>
 */
class Downloader implements DownloaderInterface
{
    /**
     * @var LoaderInterface
     */
    protected $loader;

    /**
     * Downloader constructor.
     *
     * @param LoaderInterface $loader
     */
    public function __construct(LoaderInterface $loader)
    {
        $this->loader = $loader;
    }

    /**
     * Download a dataset.
     *
     * @param string $url
     * @param string $format
     *
     * @return void
     */
    public function download($url, $format)
    {
        if (!$this->loader->support($format)) {
            // no supported format
            throw new Exception(601, sprintf('There is no support for the "%s" format', $format));
        }

        try {
            return $this->loader->load($url, $format);
        } catch (Exception $e) {
            throw $e;
        } catch (\Exception $e) {
            // unexpected exception
            throw new Exception(
                $e->getCode(),
                sprintf('The url %s trigger the following error: %s.', $url, $e->getMessage())
            );
        }
    }
}
