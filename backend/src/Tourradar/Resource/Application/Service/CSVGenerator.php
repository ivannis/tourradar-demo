<?php

/**
 * This file is part of the Tourradar application.
 *
 * Copyright (c) Tourradar
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tourradar\Resource\Application\Service;

use League\Csv\Writer;

/**
 * CSVGenerator class.
 *
 * @author Ivan Suárez Jerez <ivan@howaboutsales.com>
 */
class CSVGenerator implements GeneratorInterface
{
    /**
     * {@inheritdoc}
     */
    public function generate(array $columns, array $rows, $output, $delimiter = ',', $enclosure = ' ')
    {
        $info = pathinfo($output);
        if (!is_dir($info['dirname'])) {
            mkdir($info['dirname'], 0755, true);
        }

        // create the csv file
        $csv = Writer::createFromPath($output, "w");
        $csv->setDelimiter($delimiter);
        $csv->setEnclosure($enclosure);

        // insert the header
        $csv->insertOne($columns);

        // insert all the records
        $csv->insertAll($rows);
    }
}
