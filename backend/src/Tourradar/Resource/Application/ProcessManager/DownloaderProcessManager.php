<?php

/**
 * This file is part of the Tourradar application.
 *
 * Copyright (c) Tourradar
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tourradar\Resource\Application\ProcessManager;

use Cubiche\Domain\EventPublisher\DomainEventSubscriberInterface;
use Cubiche\Domain\ProcessManager\ProcessManager;
use Tourradar\Resource\Application\Command\DownloadDatasetCommand;
use Tourradar\Resource\Application\Command\NotifyErrorCommand;
use Tourradar\Resource\Domain\Event\DatasetDownloadFailed;
use Tourradar\Resource\Domain\Event\DatasetDownloadWasRequested;

/**
 * DownloaderProcessManager class.
 *
 * @author Ivan Suarez Jerez <ivannis.suarez@gmail.com>
 */
class DownloaderProcessManager extends ProcessManager implements DomainEventSubscriberInterface
{
    /**
     * @param DatasetDownloadWasRequested $event
     */
    public function whenDatasetDownloadWasRequested(DatasetDownloadWasRequested $event)
    {
        $this->dispatch(
            new DownloadDatasetCommand(
                $event->datasetId()->toNative(),
                $event->format()->toNative(),
                $event->output()->toNative()
            )
        );
    }

    /**
     * @param DatasetDownloadFailed $event
     */
    public function whenDatasetDownloadFailed(DatasetDownloadFailed $event)
    {
        $this->dispatch(
            new NotifyErrorCommand(
                $event->reason()->toNative(),
                array(
                    'format' => $event->format()->toNative(),
                    'errorCode' => $event->errorCode()->toNative(),
                )
            )
        );
    }

    /**
     * {@inheritdoc}
     */
    protected function name()
    {
        return 'app.process_manager.downloader';
    }

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return array(
            DatasetDownloadWasRequested::class => array('whenDatasetDownloadWasRequested', 250),
            DatasetDownloadFailed::class => array('whenDatasetDownloadFailed', 250),
        );
    }
}
