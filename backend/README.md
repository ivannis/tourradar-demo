# Backend api

The backend project contains a graphql endpoints that will be used by the [frontend](https://gitlab.com/ivannis/tourradar-demo/blob/master/frontend) application.

- http://api.tourradar.local/graphql

In development mode you will have a graphiql instance to play with it:

* [http://api.tourradar.local/graphiql](http://api.tourradar.local/graphiql)

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for testing purposes.

### Prerequisites

Add the development domain names in the host machine

```bash
# Get your current ip and add it to the hosts file
sudo pico /etc/hosts
your.current.ip.address tourradar.local www.tourradar.local api.tourradar.local coverage.tourradar.local mongodb.tourradar.local
```

### Installing

```bash
cd tourradar-demo
docker-compose up -d
docker exec -it tourradar-demo /www-data-shell
# now you are inside the tourradar-demo container
composer global require "hirak/prestissimo:^0.3"
cd /var/www/backend
composer install --prefer-dist
# IMPORTANT NOTE: It would be advisable to change the app.notification_email parameter to one that can be accessed by you
# this is the email that the application will use to send notification messages
# for the rest of the parameters you can press enter
bin/console app:install
```

The ```app:install``` command creates the data fixtures to be able to start using the application. A list of languages, three datasets, two roles (ADMIN, MARKETING) and two users are created:

- Username: **admin** Password: **admin**
- Username: **marketing** Password: **marketing**
- Dataset: **Demographic Statistics By Zip Code** Formats: **json, xml, csv, tsv**
- Dataset: **National Health and Nutrition Examination Survey** Formats: **json, xml, csv, tsv**
- Dataset: **Current Employee Names, Salaries, and Position Titles** Formats: **json, xml, csv, tsv**

## What can we do?

You have a sets of command to pull the data from a given dataset.

### Download one or multiple datasets

> *Pulls data from a list of JSON sources and converts each JSON file to CSV and saves it to disk.*

Use the following commands:

```bash
- bin/console app:dataset-download --format=some-format --output=output-directory
- bin/console app:dataset-download-all --format=some-format --output=output-directory
```

### Download multiple datasets regularly

> *Regularly checks for updates so that if the JSON source has changed, the CSV file on the disk should be automatically updated*
 
 Use the following command:

```bash
- bin/console app:dataset-download-all --format=some-format --output=output-directory --count=COUNT --interval=SECONDS
```

### Create a new datasource

> *Has a relatively easy way to add new JSON sources, ideally without even touching your code*

Use the following command:

```bash
- bin/console app:dataset-create dataset-name format,http://the-format-url.com
```

### Error checking

> *Extra (optional): Have the system protect against human errors*

You can play with the console commands and try to enter invalid formats, urls ...

### Logging system

> *Extra (optional): Have logging so you can debug and let the marketing team know if something breaks*

The best place to take a look to the log system is in the [frontend](https://gitlab.com/ivannis/tourradar-demo/blob/master/frontend) application, but if you wants you can try making some queries to the graphql API:

```gql
    query logs {
        logs {
            id
            dataset {
              name
            }
            level
            message
            createdAt
        }
    }
```

### Notification system

> *Extra (optional): Have a notification system if the update fails or the link is broken/inaccessible*

If something went wrong during the download, you will receive an email with a summary of what happened. 

**Suggestion:**
> Go to the https://httpstat.us/ website, select some status code urls and create a new dataset with those urls, eg:
> bin/console app:dataset-create foo json,https://httpstat.us/404 xml,https://httpstat.us/400 
> Then, you can try to download the foo dataset. 

### TODO 

> *Extra (optional): Have support of JSON files of different structures*
> *Extra (optional): Have other data formats supported (e.g. CSV, TSV)*

### How to use the graphql API?

The flow to be able to make a graphql query or mutation is the following:

- Go to the graphiql endpoint (http://api.tourradar.local/graphiql)
- You need a user (you can use the "admin" or "marketing" users or you can create it using the [command line]())
- You can make login using the user credentials (you will get a Json Web Token)
- You can make queries or mutations now filling the token input in graphiql using the JWT

## Tests

I used a full-stack Behavior-Driven-Development, with [atoum](http://atoum.org/) and [Behat](http://behat.org). In the root directory, you will find a ```.quality``` file, where you can define the code quality rules and all your unit tests suite.

### Running all the unit tests suite

```bash
bin/test-unit
# Or if you prefer the tests with colored output 
bin/atoum 
```

### Running all the tests coverage

```bash
# Run the tests coverage
# Make sure the /var/www/coverage directory is world writable
bin/test-coverage
# Check it at coverage.tourradar.local
```

### Running the end to end tests

```
bin/behat
```

## Built With

* [Symfony](http://symfony.com/) - The base web framework
* [Cubiche](https://github.com/cubiche/cubiche) - DDD library

## Authors

* [Ivan Suarez Jerez](https://github.com/ivannis)